#!/usr/bin/env python3
'''
EffectorPredictor command test class
'''
import unittest
import os
import shutil
from ingenannot.main import ArgParse
from ingenannot.commands.effector_predictor_cmd import EffectorPredictorCmd
from tests.ingenannot_test import IngenannotTest

class TestEffectorPredictorCmd(IngenannotTest):
    '''
    TestEffectorPredictorCmd
    '''

    def tearDown(self):

        if os.path.isdir('effpred'):
            shutil.rmtree('effpred', ignore_errors=True)
        if os.path.isfile("effectors.txt"):
            os.remove("effectors.txt")

    def test_run(self):
        '''test basic run'''

        fasta = "{}/effpred.proteins.fasta".format(self.data_dir)
        parser = ArgParse()
        sp = parser.subparser_effector_predictor()
        args = sp.parse_args([fasta, "--tmhmm",
            "/usr/local/bin/tmhmm-2.0c/bin/tmhmm" , "--effectorp",
            "/usr/local/bin/EffectorP_2.0/Scripts/EffectorP.py"])
        inst = EffectorPredictorCmd(args)
        if inst.validate_tools_status == 0:
            res = inst.run()
            observed = "effectors.txt"
            expected = "{}/effpred.out".format(self.data_dir)
            self.assertTrue(self.are_same_files(expected,observed))
        else: # Exception tools
            self.assertRaises(Exception, inst.run)

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestEffectorPredictorCmd)
    unittest.TextTestRunner(verbosity=2).run(suite)
