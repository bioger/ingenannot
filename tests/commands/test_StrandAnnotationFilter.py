#!/usr/bin/env python3
'''
Strand annotation filter command test class
'''
import unittest
import os
#import logging
from ingenannot.commands.strand_annotation_filter import StrandAnnotationFilter
from ingenannot.main import ArgParse
from tests.ingenannot_test import IngenannotTest


class TestStrandAnnotationFilter(IngenannotTest):
    '''
    TestStrandAnnotationFilter
    '''

    def tearDown(self):

        os.remove("aedstr.filter.gff")

    def test_run(self):
        '''test run'''

#        logging.getLogger().setLevel("DEBUG")
        f = "{}/select.cdsoverlap.eugene.gff".format(self.data_dir)
        output = "aedstr.filter.gff"
        parser = ArgParse()
        sp = parser.subparser_aed_strand_annotation_filter()
        args = sp.parse_args([f, output])
        inst = StrandAnnotationFilter(args)
        status = inst.run()

        expected = "{}/aedstr.filter.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestStrandAnnotationFilter)
    unittest.TextTestRunner(verbosity=2).run(suite)
