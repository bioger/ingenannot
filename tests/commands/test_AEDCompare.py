#!/usr/bin/env python3
'''
AEDCompare command test class
'''
import unittest
import os
import pandas as pd
from ingenannot.commands.aed_compare import AEDCompare
from ingenannot.main import ArgParse
from ingenannot.utils import Utils
from tests.ingenannot_test import IngenannotTest

class TestAEDCompare(IngenannotTest):
    '''
    TestAEDCompare
    '''

    def tearDown(self):
        try:
            os.remove("fof.cfg")
            os.remove("cumulative_tr_AED.png")
            os.remove("cumulative_pr_AED.png")
            os.remove("cumulative_best_AED.png")
        except OSError:
            pass

    def test_compare(self):
        '''pass'''

        fof = self.write_fof(self.data_dir, [
        ("select.cdsoverlap.CURTIN.4.gff","CURTIN"),
        ("select.cdsoverlap.MPI.4.gff","MPI"),
        ("select.cdsoverlap.eugene.4.gff","eugene"),
        ("select.cdsoverlap.JGI.4.gff","JGI"),
        ("select.cdsoverlap.RRES.4.gff","RRES")])
        cumul_tr = "cumulative_tr_AED.png"
        cumul_pr = "cumulative_pr_AED.png"
        cumul_best = "cumulative_best_AED.png"
        exp_cumul_tr = "{}/aedcomp.cumulative_tr_AED.png".format(self.data_dir)
        exp_cumul_pr = "{}/aedcomp.cumulative_pr_AED.png".format(self.data_dir)
        exp_cumul_best = "{}/aedcomp.cumulative_best_AED.png".format(self.data_dir)

        parser = ArgParse()
        sp = parser.subparser_aed_compare()
        args = sp.parse_args([fof])

        inst = AEDCompare(args)
        status = inst.run()
        self.assertEqual(status, 0)
        self.assertTrue(self.are_same_image(exp_cumul_tr,cumul_tr))
        self.assertTrue(self.are_same_image(exp_cumul_pr,cumul_pr))
        self.assertTrue(self.are_same_image(exp_cumul_best,cumul_best))

    def test_compare_with_stats(self):
        '''pass'''

        fof = self.write_fof(self.data_dir, [
        ("select.cdsoverlap.CURTIN.4.gff","CURTIN"),
        ("select.cdsoverlap.MPI.4.gff","MPI"),
        ("select.cdsoverlap.eugene.4.gff","eugene"),
        ("select.cdsoverlap.JGI.4.gff","JGI"),
        ("select.cdsoverlap.RRES.4.gff","RRES")])
        cumul_tr = "cumulative_tr_AED.png"
        cumul_pr = "cumulative_pr_AED.png"
        cumul_best = "cumulative_best_AED.png"
        exp_cumul_tr = "{}/aedcomp.cumulative_tr_AED.png".format(self.data_dir)
        exp_cumul_pr = "{}/aedcomp.cumulative_pr_AED.png".format(self.data_dir)
        exp_cumul_best = "{}/aedcomp.cumulative_best_AED.png".format(self.data_dir)

        parser = ArgParse()
        sp = parser.subparser_aed_compare()
        args = sp.parse_args([fof,"-s"])

        inst = AEDCompare(args)
        status = inst.run()
        self.assertEqual(status, 0)
        self.assertTrue(self.are_same_image(exp_cumul_tr,cumul_tr))
        self.assertTrue(self.are_same_image(exp_cumul_pr,cumul_pr))
        self.assertTrue(self.are_same_image(exp_cumul_best,cumul_best))

    def test_perform_stats(self):
        '''test stats function'''

        fof = self.write_fof(self.data_dir, [
        ("select.cdsoverlap.CURTIN.4.gff","CURTIN")])

        parser = ArgParse()
        sp = parser.subparser_aed_compare()
        args = sp.parse_args([fof,"-s"])

        inst = AEDCompare(args)
        sources = Utils.get_sources_from_fof(fof)
        genes = Utils.extract_genes_from_fof(fof)
        Utils.get_aed_from_attributes(genes)
        dfstats = inst.perform_stats(sources, genes)
        expdfstats = pd.DataFrame([[0.208,0.208,0.171,0.517,0.517,0.483,0.208,0.517,0.512,0.56,0.512,0.56]],columns=['mean (tr)','median (tr)','stdev (tr)','mean (pr)','median (pr)','stdev (pr)', 'median_geom_x','median_geom_y','mean_distance', 'mean_distance_to_0','median_distance', 'median_distance_to_0'],index=['CURTIN'])
        self.assertIsNone(pd.testing.assert_frame_equal(expdfstats,dfstats))


    #@unittest.skip('pass')
    def test_missing_aed(self):
        '''test if logging error when missing aed tags'''


        fof = self.write_fof(self.data_dir, [
        ("aedcomp.noaed.CURTIN.gff","CURTIN"),
        ("select.cdsoverlap.MPI.4.gff","MPI"),
        ("select.cdsoverlap.eugene.4.gff","eugene"),
        ("select.cdsoverlap.JGI.4.gff","JGI"),
        ("select.cdsoverlap.RRES.4.gff","RRES")])
        cumul_tr = "cumulative_tr_AED.png"
        cumul_pr = "cumulative_pr_AED.png"
        cumul_best = "cumulative_best_AED.png"
        exp_cumul_tr = "{}/aedcomp.cumulative_tr_AED.2.png".format(self.data_dir)
        exp_cumul_pr = "{}/aedcomp.cumulative_pr_AED.2.png".format(self.data_dir)
        exp_cumul_best = "{}/aedcomp.cumulative_best_AED.2.png".format(self.data_dir)

        parser = ArgParse()
        sp = parser.subparser_aed_compare()
        args = sp.parse_args([fof])

        inst = AEDCompare(args)
        status = inst.run()
        self.assertEqual(status, 0)
        self.assertTrue(self.are_same_image(exp_cumul_tr,cumul_tr))
        self.assertTrue(self.are_same_image(exp_cumul_pr,cumul_pr))
        self.assertTrue(self.are_same_image(exp_cumul_best,cumul_best))

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestAEDCompare)
    unittest.TextTestRunner(verbosity=2).run(suite)
