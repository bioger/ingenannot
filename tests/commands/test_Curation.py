#!/usr/bin/env python3
'''
Curation command test class
'''
import unittest
import os
from ingenannot.commands.curation import Curation
from ingenannot.main import ArgParse
from tests.ingenannot_test import IngenannotTest

class TestCuration(IngenannotTest):
    '''
    TestCuration
    '''

    def tearDown(self):

        os.remove("curation.gff")
        os.remove("curation.png")

    def test_run(self):
        '''basic test run'''

        annots = f"{self.data_dir}/curation.ztritici.chr1.gff3"
        output = "curation.gff"
        graph_out = "curation.png"
        parser = ArgParse()
        sp =  parser.subparser_curation()
        args = sp.parse_args([annots,output])
        inst = Curation(args)
        status = inst.run()

        expected = f"{self.data_dir}/curation.gff"
        expected_graph = f"{self.data_dir}/curation.png"
        observed = output
        observed_graph = graph_out
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertTrue(self.are_same_image(expected_graph, observed_graph))
        self.assertEqual(status, 0)

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestCuration)
    unittest.TextTestRunner(verbosity=2).run(suite)
