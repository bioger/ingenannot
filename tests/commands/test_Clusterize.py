#!/usr/bin/env python3
'''
Cluterize command test class
'''
import unittest
import os
from ingenannot.commands.clusterize import Clusterize
from ingenannot.main import ArgParse
from tests.ingenannot_test import IngenannotTest

class TestClusterize(IngenannotTest):
    '''
    TestClusterize
    '''

    def tearDown(self):

        if os.path.exists('cluster.out.gff'):
            os.remove("cluster.out.gff")

    def test_run(self):
        '''basic test run'''

        f = "{}/cluster.tr.gff".format(self.data_dir)
        output = "cluster.out.gff"
        parser = ArgParse()
        sp = parser.subparser_clusterize()
        args = sp.parse_args([f,output])
        inst = Clusterize(args)
        status = inst.run()

        expected = "{}/cluster.tr.out.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

        args = sp.parse_args([f,output, "--keep_atts"])
        inst = Clusterize(args)
        status = inst.run()

        expected = "{}/cluster.tr.out.2.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

    def test_run_annot(self):
        '''test run with annots'''

        f = "{}/cluster.tr.gff".format(self.data_dir)
        output = "cluster.out.gff"
        annots = "{}/cluster.annots.gff".format(self.data_dir)
        parser = ArgParse()
        sp = parser.subparser_clusterize()
        args = sp.parse_args([f,output,"-g",annots])
        inst = Clusterize(args)
        status = inst.run()

        expected = "{}/cluster.tr.annot.out.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestClusterize)
    unittest.TextTestRunner(verbosity=2).run(suite)
