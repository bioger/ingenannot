#!/usr/bin/env python3
'''
filter command test class
'''
import unittest
import os
from ingenannot.commands.filter import Filter
from ingenannot.main import ArgParse
from tests.ingenannot_test import IngenannotTest


class TestFilter(IngenannotTest):
    '''
    TestFilter
    '''

    def tearDown(self):

        if os.path.exists('out_filter.gff'):
            os.remove('out_filter.gff')

    def test_run(self):
        '''basic test run'''

        gff_genes = '{}/filter.Cdes.gff3'.format(self.data_dir)
        gff_TEs = '{}/filter.CdesC1_refTEs_newname.gff'.format(self.data_dir)
        expected = '{}/filter.out_filter.gff'.format(self.data_dir)
        output = 'out_filter.gff'

        parser = ArgParse()
        sp = parser.subparser_filter()
        args = sp.parse_args([gff_genes, gff_TEs, output])

        inst = Filter(args)
        status = inst.run()

        self.assertTrue(self.are_same_files(expected, output))
        self.assertEqual(status, 0)

    def test_run_bed(self):
        '''test run with bed file'''

        gff_genes = '{}/filter.Cdes.gff3'.format(self.data_dir)
        bed_TEs = '{}/filter.TE.bed'.format(self.data_dir)
        expected = '{}/filter.out_filter.bed.gff'.format(self.data_dir)
        output = 'out_filter.gff'

        parser = ArgParse()
        sp = parser.subparser_filter()
        args = sp.parse_args([gff_genes, bed_TEs, output, "--bed", "--size", "10"])

        inst = Filter(args)
        status = inst.run()
        self.assertTrue(self.are_same_files(expected, output))
        self.assertEqual(status, 0)

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestFilter)
    unittest.TextTestRunner(verbosity=2).run(suite)
