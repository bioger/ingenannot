#!/usr/bin/env python3
'''
Rename command test class
'''
import unittest
import io
import os
import contextlib
from ingenannot.commands.rename import Rename
from ingenannot.main import ArgParse
from tests.ingenannot_test import IngenannotTest

class TestRename(IngenannotTest):
    '''
    TestRename
    '''

    def tearDown(self):

        try:
            os.remove("out.rename.gff")
            os.remove("mapping_transcript.txt")
            os.remove("mapping_gene.txt")
        except:
            pass


    def test_run(self):
        '''test run'''

        f = io.StringIO()
        fin = "{}/rename.gff".format(self.data_dir)
        output = "out.rename.gff"

        with contextlib.redirect_stdout(f):

            parser = ArgParse()
            sp = parser.subparser_rename()
            args = sp.parse_args([fin, "{ref}_{geneidx}"])
            inst = Rename(args)
            status = inst.run()

        with open("out.rename.gff", 'w') as fh:
            #print(f.getvalue())
            fh.write(f.getvalue())
        fh.close()

        expected = "{}/rename.out.1.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected, output))
        self.assertEqual(status, 0)
        f.close()

        f = io.StringIO()
        fin = "{}/rename.gff".format(self.data_dir)
        with contextlib.redirect_stdout(f):
            args = sp.parse_args([fin, "G{geneidx:05}0"])
            inst = Rename(args)
            status = inst.run()

        with open("out.rename.gff", 'w') as fh:
            fh.write(f.getvalue())
        fh.close()

        expected = "{}/rename.out.2.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected, output))
        self.assertEqual(status, 0)
        f.close()

        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            args = sp.parse_args([fin, "{ref}_G{geneidxref:05}"])
            inst = Rename(args)
            status = inst.run()

        with open("out.rename.gff", 'w') as fh:
            fh.write(f.getvalue())
        fh.close()

        expected = "{}/rename.out.3.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected, output))
        self.assertEqual(status, 0)
        f.close()

        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            args = sp.parse_args([fin, "{ref}_G{geneidxref:05}", "--name", "--mapping"])
            inst = Rename(args)
            status = inst.run()

        with open("out.rename.gff", 'w') as fh:
            fh.write(f.getvalue())
        fh.close()

        expected = "{}/rename.out.4.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected, output))
        # mapping files
        self.assertTrue(self.are_same_files("mapping_gene.txt",
            "{}/rename.out.4.mapping.gene.txt".format(self.data_dir)))
        self.assertTrue(self.are_same_files("mapping_transcript.txt",
            "{}/rename.out.4.mapping.tr.txt".format(self.data_dir)))
        self.assertEqual(status, 0)


    def test_run_2(self):
        '''test run'''

#        logging.getLogger().setLevel("DEBUG")
        f = io.StringIO()
        fin = "{}/atapiso.out.run1_3.gff".format(self.data_dir)
        output = "out.rename.gff"

        with contextlib.redirect_stdout(f):

            parser = ArgParse()
            sp = parser.subparser_rename()
            args = sp.parse_args([fin, "{ref}_{geneidx:05}", "--mapping"])
            inst = Rename(args)
            status = inst.run()

        with open("out.rename.gff", 'w') as fh:
            #print(f.getvalue())
            fh.write(f.getvalue())
        fh.close()

        expected = "{}/rename.out.5.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected, output))
        # mapping files
        self.assertTrue(self.are_same_files("mapping_gene.txt",
            "{}/rename.out.5.mapping.gene.txt".format(self.data_dir)))
        self.assertTrue(self.are_same_files("mapping_transcript.txt",
            "{}/rename.out.5.mapping.tr.txt".format(self.data_dir)))
        self.assertEqual(status, 0)
        f.close()

        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            args = sp.parse_args([fin, "{ref}_{geneidx:05}", "--name",
                "--tr_ranking_ID", "--locus_tag", "--mapping"])
            inst = Rename(args)
            status = inst.run()

        with open("out.rename.gff", 'w') as fh:
            fh.write(f.getvalue())
        fh.close()

        expected = "{}/rename.out.6.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected, output))
        # mapping files
        self.assertTrue(self.are_same_files("mapping_gene.txt",
            "{}/rename.out.5.mapping.gene.txt".format(self.data_dir)))
        self.assertTrue(self.are_same_files("mapping_transcript.txt",
            "{}/rename.out.6.mapping.tr.txt".format(self.data_dir)))
        self.assertEqual(status, 0)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestRename)
    unittest.TextTestRunner(verbosity=2).run(suite)
