#!/usr/bin/env python3
'''
ExonerateToGff command test class
'''
import unittest
import os
import io
import contextlib
from ingenannot.commands.exonerate_to_gff import ExonerateToGff
from ingenannot.main import ArgParse
from tests.ingenannot_test import IngenannotTest

class TestExonerateToGff(IngenannotTest):
    '''
    TestExonerateToGff
    '''

    def tearDown(self):

        os.remove("out.exo.gff")


    def test_run(self):
        '''test run'''

        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            fh = "{}/exo.exonerate.out".format(self.data_dir)
            parser = ArgParse()
            sp = parser.subparser_exonerate_to_gff()
            args = sp.parse_args([fh])
            inst = ExonerateToGff(args)
            status = inst.run()

        output = "out.exo.gff"
        with open("out.exo.gff", 'w') as fh:
            fh.write(f.getvalue())

        expected = "{}/exo.exonerate.out.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected, output))
        self.assertEqual(status, 0)

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestExonerateToGff)
    unittest.TextTestRunner(verbosity=2).run(suite)
