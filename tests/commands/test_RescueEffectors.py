#!/usr/bin/env python3
'''
RescueEffectors command test class
'''
import unittest
import os
import shutil
from ingenannot.main import ArgParse
from ingenannot.commands.rescue_effectors import RescueEffectors
from ingenannot.utils.effector_predictor import EffectorPredictor
from tests.ingenannot_test import IngenannotTest
import logging

class TestRescueEffectors(IngenannotTest):
    '''
    TestRescueEffectors
    '''

    def setUp(self):

        logging.getLogger().setLevel('INFO' )
        # set path to programs
        ## Genobioinfo
        self.signalp = "/usr/local/bioinfo/src/SignalP/signalp-4.1/signalp"
        self.tmhmm = "/usr/local/bioinfo/src/TMHMM/tmhmm-2.0c/bin/tmhmm" 
        self.targetp = "/usr/local/bioinfo/src/TargetP/targetp-2.0/bin/targetp"
        self.effectorp = "/home/nlapalu/work/tools/EffectorP-2.0-2.0.1/Scripts/EffectorP.py"
 
    def tearDown(self):

        try:
            os.remove("rescue_eff_prot.fasta")
            #os.remove("effectors.gff3")
            shutil.rmtree('effpred')
        except OSError:
            print("Problem in cleaning data in the test environment.")

    @unittest.skip('genobioinfo only')
    def test_run_0(self):
        '''test run with '''

        gff_genes = "{}/rescue.genes.gff3".format(self.data_dir) # merge with sringtie merge
        gff_tr = "{}/rescue.transcripts.2.gff.gz".format(self.data_dir) # merge with sringtie merge
        genome = "{}/rescue.genome.fasta.gz".format(self.data_dir)

        parser = ArgParse()
        sp = parser.subparser_rescue_effectors()
        args = sp.parse_args([gff_genes, gff_tr, genome,"--signalp", self.signalp,"--tmhmm",self.tmhmm,"--targetp",self.targetp, "--effectorp", self.effectorp])
        inst = RescueEffectors(args)
        status = inst.run()
        observed = "effectors.gff3"
        expected = "{}/rescue.effectors.0.gff3".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)
        
    @unittest.skip('genobioinfo only')
    def test_run_1(self):
        '''test run with nested'''

        gff_genes = "{}/rescue.genes.gff3".format(self.data_dir) # merge with sringtie merge
        gff_tr = "{}/rescue.transcripts.2.gff.gz".format(self.data_dir) # merge with sringtie merge
        genome = "{}/rescue.genome.fasta.gz".format(self.data_dir)

        parser = ArgParse()
        sp = parser.subparser_rescue_effectors()
        args = sp.parse_args([gff_genes, gff_tr, genome,"--signalp", self.signalp,"--tmhmm",self.tmhmm,"--targetp",self.targetp, "--effectorp", self.effectorp,"--nested"])
        inst = RescueEffectors(args)
        status = inst.run()
        observed = "effectors.gff3"
        expected = "{}/rescue.effectors.1.gff3".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

    @unittest.skip('genobioinfo only')
    def test_run_2(self):
        '''test run 2'''

        gff_genes = "{}/rescue.genes.gff3".format(self.data_dir) # merge with sringtie merge
        genome = "{}/rescue.genome.fasta.gz".format(self.data_dir)
        gff_tr = "{}/rescue.tr.gff.gz".format(self.data_dir)

        parser = ArgParse()
        sp = parser.subparser_rescue_effectors()
        args = sp.parse_args([gff_genes, gff_tr, genome, "--signalp", self.signalp,"--tmhmm",self.tmhmm,"--targetp",self.targetp,"--effectorp", self.effectorp])
        inst = RescueEffectors(args)
        observed = "effectors.gff3"
        expected = "{}/rescue.effectors.2.gff3".format(self.data_dir)
        status = inst.run()
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

    @unittest.skip('genobioinfo only')
    def test_run_3(self):
        '''test run 3: with new data'''

        gff_genes = "{}/genes.rescue.3.gff".format(self.data_dir) # merge with sringtie merge
        genome = "{}/genome.rescue.3.fasta.gz".format(self.data_dir)
        gff_tr = "{}/tr.rescue.3.gff.gz".format(self.data_dir)

        parser = ArgParse()
        sp = parser.subparser_rescue_effectors()
        args = sp.parse_args([gff_genes, gff_tr, genome, "--signalp", self.signalp,"--tmhmm",self.tmhmm,"--targetp",self.targetp,"--effectorp", self.effectorp])
        inst = RescueEffectors(args)
        observed = "effectors.gff3"
        expected = "{}/rescue.effectors.3.gff3".format(self.data_dir)
        status = inst.run()
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

    @unittest.skip('genobioinfo only')
    def test_run_4(self):
        '''test run 4: with new data, test nested'''

        gff_genes = "{}/genes.rescue.3.gff".format(self.data_dir) # merge with sringtie merge
        genome = "{}/genome.rescue.3.fasta.gz".format(self.data_dir)
        gff_tr = "{}/tr.rescue.3.gff.gz".format(self.data_dir)

        parser = ArgParse()
        sp = parser.subparser_rescue_effectors()
        args = sp.parse_args([gff_genes, gff_tr, genome, "--signalp", self.signalp,"--tmhmm",self.tmhmm,"--targetp",self.targetp,"--effectorp", self.effectorp, "--nested"])
        inst = RescueEffectors(args)
        observed = "effectors.gff3"
        expected = "{}/rescue.effectors.3.gff3".format(self.data_dir)
        status = inst.run()
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

    @unittest.skip('genobioinfo only')
    def test_run_5(self):
        '''test run 5: with new data, test unstranded'''

        gff_genes = "{}/genes.rescue.3.gff".format(self.data_dir) # merge with sringtie merge
        genome = "{}/genome.rescue.3.fasta.gz".format(self.data_dir)
        gff_tr = "{}/tr.rescue.3.gff.gz".format(self.data_dir)

        parser = ArgParse()
        sp = parser.subparser_rescue_effectors()
        args = sp.parse_args([gff_genes, gff_tr, genome, "--signalp", self.signalp,"--tmhmm",self.tmhmm,"--targetp",self.targetp,"--effectorp", self.effectorp, "--unstranded"])
        inst = RescueEffectors(args)
        observed = "effectors.gff3"
        expected = "{}/rescue.effectors.4.gff3".format(self.data_dir)
        status = inst.run()
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

    @unittest.skip('genobioinfo only')
    def test_run_6(self):
        '''test run 6: with new data, test unstranded and nested'''

        gff_genes = "{}/genes.rescue.3.gff".format(self.data_dir) # merge with sringtie merge
        genome = "{}/genome.rescue.3.fasta.gz".format(self.data_dir)
        gff_tr = "{}/tr.rescue.3.gff.gz".format(self.data_dir)

        parser = ArgParse()
        sp = parser.subparser_rescue_effectors()
        args = sp.parse_args([gff_genes, gff_tr, genome, "--signalp", self.signalp,"--tmhmm",self.tmhmm,"--targetp",self.targetp,"--effectorp", self.effectorp, "--unstranded", "--nested"])
        inst = RescueEffectors(args)
        observed = "effectors.gff3"
        expected = "{}/rescue.effectors.5.gff3".format(self.data_dir)
        status = inst.run()
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestRescueEffectors)
    unittest.TextTestRunner(verbosity=2).run(suite)
