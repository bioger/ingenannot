#!/usr/bin/env python3
'''
SOClassification command test class
'''
import unittest
import os
import glob
from ingenannot.commands.so_classification import SOClassification
from ingenannot.main import ArgParse
from tests.ingenannot_test import IngenannotTest

class TestSOClassification(IngenannotTest):
    '''
    TestSOClassification
    '''

    def tearDown(self):

        gff3list = glob.glob('*.gff3')
        for f in gff3list:
            os.remove(f)

        if os.path.exists('fof.cfg'):
            os.remove('fof.cfg')


    def test_run(self):
        '''test run'''

        fof = self.write_fof(self.data_dir,[('validate.Cdes.gff3','eugene'),
            ('validate.fungap.gff3','fungap')])
        parser = ArgParse()
        sp = parser.subparser_soclassif()
        args = sp.parse_args([fof])
        inst = SOClassification(args)
        status = inst.run()
        self.assertEqual(status, 0)

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestSOClassification)
    unittest.TextTestRunner(verbosity=2).run(suite)
