#!/usr/bin/env python3
'''
UTRRefine command test class
'''
import unittest
import os
from ingenannot.commands.utr_refine import UTRRefine
from ingenannot.main import ArgParse
from tests.ingenannot_test import IngenannotTest

class TestUTRRefine(IngenannotTest):
    '''
    TestUTRRefine
    '''

    def tearDown(self):

        if os.path.exists('out_utr_refine.gff'):
            os.remove('out_utr_refine.gff')

    def test_run_1(self):
        '''test all different UTR mode'''

        parser = ArgParse()
        sp = parser.subparser_utr_refine()

        ## all utr isoform
        gff_genes = '{}/utrrefine.annotation.gff'.format(self.data_dir)
        gff_transcripts = '{}/utrrefine.transcripts.run1_1.gff'.format(self.data_dir)
        gff_output = 'out_utr_refine.gff'

        args = sp.parse_args([gff_genes, gff_transcripts, gff_output, '--utr_mode', 'all'])
        #logging.getLogger().setLevel('DEBUG')
        inst = UTRRefine(args)
        status = inst.run()
        expected = "{}/utrrefine.out.run1_1.gff".format(self.data_dir)
        observed = gff_output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

        ## longest
        args = sp.parse_args([gff_genes, gff_transcripts, gff_output, '--utr_mode', 'longest'])
        inst = UTRRefine(args)
        status = inst.run()
        expected = "{}/utrrefine.out.run1_2.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected, observed))

        ## shortest
        args = sp.parse_args([gff_genes, gff_transcripts, gff_output, '--utr_mode', 'shortest'])
        inst = UTRRefine(args)
        status = inst.run()
        expected = "{}/utrrefine.out.run1_3.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected, observed))

        ## rank Exception
        args = sp.parse_args([gff_genes, gff_transcripts, gff_output, '--utr_mode', 'rank'])
        inst = UTRRefine(args)
        self.assertRaises(Exception,inst.run)

        ## rank
        gff_transcripts = '{}/utrrefine.transcripts.run1_2.gff'.format(self.data_dir)
        args = sp.parse_args([gff_genes, gff_transcripts, gff_output, '--utr_mode', 'rank'])
        inst = UTRRefine(args)
        status = inst.run()
        expected = "{}/utrrefine.out.run1_4.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected, observed))


    def test_run_2(self):
        '''test erase mode'''

        parser = ArgParse()
        sp = parser.subparser_utr_refine()

        gff_genes = '{}/utrrefine.out.run1_2.gff'.format(self.data_dir)
        gff_transcripts = '{}/utrrefine.transcripts.run1_1.gff'.format(self.data_dir)
        gff_output = 'out_utr_refine.gff'

        ## shortest
        args = sp.parse_args([gff_genes, gff_transcripts, gff_output,
            '--utr_mode', 'shortest', '--erase'])
        inst = UTRRefine(args)
        status = inst.run()
        expected = "{}/utrrefine.out.run1_3.gff".format(self.data_dir)
        observed = gff_output
        self.assertTrue(self.are_same_files(expected, observed))

        # multiple CDS overlap / same transcript with UTR for several mRNA -> no UTR annotation
        gff_genes = '{}/utrrefine.out.run1_1.gff'.format(self.data_dir)

        args = sp.parse_args([gff_genes, gff_transcripts, gff_output,
            '--utr_mode', 'shortest', '--erase'])
        inst = UTRRefine(args)
        status = inst.run()
        expected = "{}/utrrefine.out.run2_1.gff".format(self.data_dir)
        observed = gff_output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)


    def test_run_3(self):
        '''test only new mode'''

        parser = ArgParse()
        sp = parser.subparser_utr_refine()

        gff_genes = '{}/utrrefine.out.run1_2.gff'.format(self.data_dir)
        gff_transcripts = '{}/utrrefine.transcripts.run1_1.gff'.format(self.data_dir)
        gff_output = 'out_utr_refine.gff'

        ## shortest
        args = sp.parse_args([gff_genes, gff_transcripts, gff_output,
            '--utr_mode', 'shortest', '--onlynew'])
        inst = UTRRefine(args)
        status = inst.run()
        expected = "{}/utrrefine.out.run1_2.gff".format(self.data_dir)
        observed = gff_output
        self.assertTrue(self.are_same_files(expected, observed))

        # add 1 new isoform / control case false intron
        gff_genes = '{}/utrrefine.annotation_3.gff'.format(self.data_dir)
        gff_transcripts = '{}/utrrefine.transcripts.run1_1.gff'.format(self.data_dir)
        gff_output = 'out_utr_refine.gff'

        ## shortest
        args = sp.parse_args([gff_genes, gff_transcripts, gff_output, '--utr_mode', 'shortest'])
        inst = UTRRefine(args)
        status = inst.run()
        expected = "{}/utrrefine.out.run3_1.gff".format(self.data_dir)
        observed = gff_output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)


    def test_run_4(self):
        '''test debug'''

        parser = ArgParse()
        sp = parser.subparser_utr_refine()

        gff_genes = '{}/utrrefine.annotation_4.gff'.format(self.data_dir)
        gff_transcripts = '{}/utrrefine.transcripts.run4.gff'.format(self.data_dir)
        gff_output = 'out_utr_refine.gff'

        ## shortest
        args = sp.parse_args([gff_genes, gff_transcripts, gff_output, '--utr_mode', 'shortest'])
        inst = UTRRefine(args)
        status = inst.run()
        expected = "{}/utrrefine.out.run4.gff".format(self.data_dir)
        observed = gff_output
        self.assertTrue(self.are_same_files(expected, observed))

        gff_transcripts = '{}/isorank.tr.isoform.ranking.gff'.format(self.data_dir)
        ## shortest
        args = sp.parse_args([gff_genes, gff_transcripts, gff_output, '--utr_mode', 'shortest'])
        inst = UTRRefine(args)
        status = inst.run()
        expected = "{}/utrrefine.out.run4_2.gff".format(self.data_dir)
        observed = gff_output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestUTRRefine)
    unittest.TextTestRunner(verbosity=2).run(suite)
