#!/usr/bin/env python3
'''
AED command test class
'''
import unittest
import os
from ingenannot.commands.aed import AED
from ingenannot.main import ArgParse
from tests.ingenannot_test import IngenannotTest

class TestAED(IngenannotTest):
    '''
    TestAED
    '''

    def tearDown(self):

        os.remove("select.gff")
        os.remove("scatter_hist_aed.CURTIN.png")

    def test_run(self):
        '''basic test run'''

        f = "{}/aed.CURTIN.gff".format(self.data_dir)
        source = "CURTIN"
        output = "select.gff"
        evtr = "{}/evidence-rnaseq.gff.gz".format(self.data_dir)
        evpr = "{}/evidence-protein.gff.gz".format(self.data_dir)
        evlg = "{}/evidence-isoseq.gff.gz".format(self.data_dir)
        parser = ArgParse()
        sp = parser.subparser_aed()
        args = sp.parse_args([f,output,source,evtr,evpr, "--evtrstranded",
            "--evprstranded", "--aedtr","0.2","--aedpr","0.1"])
        inst = AED(args)
        status = inst.run()

        expected = "{}/aed.CURTIN.res.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

        args = sp.parse_args([f,output,source,evtr,evpr, "--evtrstranded",
            "--evprstranded", "--aedtr","0.2","--aedpr","0.1", "--longreads",
            evlg])
        inst = AED(args)
        status = inst.run()

        expected = "{}/aed.CURTIN.res.lg.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

    def test_run_evpr_source(self):
        '''use miniprot as input for protein'''

        f = "{}/aed.CURTIN.gff".format(self.data_dir)
        source = "CURTIN"
        output = "select.gff"
        evtr = "{}/evidence-rnaseq.gff.gz".format(self.data_dir)
        #evpr = "{}/evidence-protein.gff.gz".format(self.data_dir)
        evpr = "{}/evidence-protein.miniprot.gff.gz".format(self.data_dir)
        evlg = "{}/evidence-isoseq.gff.gz".format(self.data_dir)
        parser = ArgParse()
        sp = parser.subparser_aed()
        args = sp.parse_args([f,output,source,evtr,evpr, "--evtrstranded",
            "--evprstranded", "--aedtr","0.2","--aedpr","0.1", "--evpr_source","miniprot"])
        inst = AED(args)
        status = inst.run()

        expected = "{}/aed.CURTIN.res.miniprot.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestAED)
    unittest.TextTestRunner(verbosity=2).run(suite)
