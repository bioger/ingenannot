#!/usr/bin/env python3
'''
AddSqanti3Isoforms command test class
'''
import os
import unittest
from ingenannot.commands.add_sqanti3_isoforms import AddSqanti3Isoforms
from ingenannot.main import ArgParse
from tests.ingenannot_test import IngenannotTest


class TestAddSqanti3Isoforms (IngenannotTest):
    '''
    TestAddSqanti3Isoforms
    '''

    def tearDown(self):

        if os.path.exists('out_add_tapas_isoforms.gff'):
            os.remove('out_add_tapas_isoforms.gff')

    def test_run (self):
        '''basic test run'''

        gff_genes = '{}/atapiso.genes.gff'.format(self.data_dir)
        gff_transcripts = '{}/atapiso.transcripts.gff'.format(self.data_dir)
        l_IDs = "{}/atapiso.lIDs.txt".format(self.data_dir)
        gff_output = 'out_add_tapas_isoforms.gff'
        parser = ArgParse()
        sp = parser.subparser_add_sqanti3_isoforms()
        args = sp.parse_args([gff_genes, gff_transcripts, l_IDs, '-o', gff_output])
        #logging.getLogger().setLevel('DEBUG')
        inst = AddSqanti3Isoforms(args)
        status = inst.run()
        observed = gff_output
        expected = "{}/atapiso.out.run1_1.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

        args = sp.parse_args([gff_genes, gff_transcripts, l_IDs, '-o',
            gff_output, '--no_identicals'])
        inst = AddSqanti3Isoforms(args)
        status = inst.run()
        expected = "{}/atapiso.out.run1_2.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

        # test Warning bad isoform / bad output
        gff_transcripts = '{}/atapiso.transcripts.2.gff'.format(self.data_dir)
        args = sp.parse_args([gff_genes, gff_transcripts, l_IDs, '-o',
            gff_output, '--no_identicals'])
        inst = AddSqanti3Isoforms(args)
        status = inst.run()
        expected = "{}/atapiso.out.run1_3.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestAddSqanti3Isoforms)
    unittest.TextTestRunner(verbosity=2).run(suite)
