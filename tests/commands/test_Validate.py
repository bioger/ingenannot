#!/usr/bin/env python3
'''
Validate command test class
'''
import unittest
import os
from ingenannot.main import ArgParse
from ingenannot.commands.validate import Validate
from tests.ingenannot_test import IngenannotTest

class TestValidate(IngenannotTest):
    '''
    TestValidate
    '''

    def tearDown(self):

        if os.path.isfile("genes.gff"):
            os.remove("genes.gff")

    def test_run_gff3_eugene(self):
        '''test_run_gff3_eugene'''

        gff_genes = '{}/validate.Cdes.gff3'.format(self.data_dir)
        output = "genes.gff"

        parser = ArgParse()
        sbp = parser.subparser_validate()
        args = sbp.parse_args([gff_genes, "-o", output])
        inst = Validate(args)
        status = inst.run()

        expected = "{}/validate.Cdes.res.gff3".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

    def test_run_gff3_fungap(self):
        '''test_run_gff3_fungap'''

        gff_genes = '{}/validate.fungap.gff3'.format(self.data_dir)
        output = "genes.gff"

        parser = ArgParse()
        sbp = parser.subparser_validate()
        args = sbp.parse_args([gff_genes, "--fixframe"])
        inst = Validate(args)
        status = inst.run()

        args = sbp.parse_args([gff_genes, "--fixframe", "--gaeval"])
        inst = Validate(args)
        status = inst.run()

        args = sbp.parse_args([gff_genes, "-o", output, "--fixframe",
            "--gaeval", "--statistics"])
        inst = Validate(args)
        status = inst.run()

        expected = "{}/validate.fungap.res.gff3".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

    def test_run_gff3_fungap_exception(self):
        '''test_run_gff3_fungap_exception'''

        gff_genes = '{}/validate.fungap.error.gff3'.format(self.data_dir)

        parser = ArgParse()
        sbp = parser.subparser_validate()
        args = sbp.parse_args([gff_genes, "--fixframe"])
        inst = Validate(args)
        self.assertRaises(Exception,inst.run())

    def test_run_gff_braker_gtf(self):
        '''test run_gff_braker_gtf'''

        gff_genes = '{}/validate.braker.gtf'.format(self.data_dir)
        output = "genes.gff"

        parser = ArgParse()
        sbp = parser.subparser_validate()
        args = sbp.parse_args([gff_genes, "-o", output, "-s"])
        inst = Validate(args)
        status = inst.run()

        expected = "{}/validate.braker.res.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

    def test_run_gff_braker_gtf_exception(self):
        '''test run_gff_braker_gtf_exception'''

        gff_genes = '{}/validate.braker.error.gff'.format(self.data_dir)
        output = "genes.gff"

        parser = ArgParse()
        sbp = parser.subparser_validate()
        args = sbp.parse_args([gff_genes, "-o", output])
        inst = Validate(args)
        status = inst.run()
        self.assertRaises(Exception,inst.run())


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestValidate)
    unittest.TextTestRunner(verbosity=2).run(suite)
