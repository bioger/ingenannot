#!/usr/bin/env python3
'''
Compare command test class
'''
import unittest
import os
from ingenannot.commands.compare import Compare
from ingenannot.main import ArgParse
from ingenannot.utils import Utils
from tests.ingenannot_test import IngenannotTest


class TestCompare(IngenannotTest):
    '''
    TestCompare
    '''

    def tearDown(self):

        try:
            os.remove("fof.cfg")
            os.remove("venn_src1.list")
            os.remove("venn_src2.list")
            os.remove("all_sources_venn.list")
            os.remove("upsetplot.png")
            os.remove("same_cds.gff3")
            os.remove("src1.specific_CDS_not_loci.gff")
            os.remove("src1.specific_loci.gff")
            os.remove("src2.specific_CDS_not_loci.gff")
            os.remove("src2.specific_loci.gff")
        except OSError:
            pass

    def test_compare(self):
        '''basic test run'''

        fof = self.write_fof(self.data_dir, [('compare.src1.gff3','src1'),
            ('compare.src2.gff','src2')])
        parser = ArgParse()
        sp = parser.subparser_compare()
        args = sp.parse_args([fof])
        inst = Compare(args)

        sources = Utils.get_sources_from_fof(fof)
        genes = Utils.extract_genes_from_fof(fof)
        clusters = Utils.clusterize(genes, cltype='cds', stranded=False, procs=1)
        metagenes = Utils.get_metagenes_from_clusters(clusters)
        all_CDS, CDS_clu_sources, metrics =  inst.compare(metagenes, sources)

        expected_metrics = \
        {'Number of Metagenes': 22,
         'Number of different CDS': 24,
         'Number of sources per CDS': {1: 21, 2: 3},
         'Number of CDS shared by all sources': 3,
         'Number of MetaGenes with unique CDS': 20,
         'Number of MetaGenes with unique CDS (nb sources)': {1: 17, 2: 3},
         'Number of MetaGenes with multiple CDS': 2,
         'Number of MetaGenes with multiple CDS (nb different CDS)': {2: 2},
         'shared same CDS': ['src1 - src1: 10', 'src1 - src2: 3', 'src2 - src2: 17'],
         'Number of specific CDS per source': {'src1': 7, 'src2': 14},
         'Number of specific CDS, with other CDS from other source at \
the same locus/metagene': {'src1': 1, 'src2': 1},
         'Number of specific loci/Metagene per source': {'src1': 6, 'src2': 12},
         'Number of sources of most representative CDS per Metagene': {1: 19, 2: 3}}

        self.assertDictEqual(expected_metrics, metrics)


    def test_run(self):
        '''test run with option'''

        fof = self.write_fof(self.data_dir, [('compare.src1.gff3','src1'),
            ('compare.src2.gff','src2')])
        parser = ArgParse()
        sp = parser.subparser_compare()
        args = sp.parse_args([fof, "--export_same_cds", "--export_venn",
            "--export_upsetplot", "--export_specific"])
        inst = Compare(args)
        status = inst.run()
        self.assertEqual(status, 0)

        # same CDS
        expected = "{}/compare.same_cds.gff3".format(self.data_dir)
        observed = "same_cds.gff3"
        self.assertTrue(self.are_same_files(expected, observed))

        # check venn list files
        expected = "{}/compare.venn_src1.list".format(self.data_dir)
        observed = "venn_src1.list"
        self.assertTrue(self.are_same_files(expected, observed))
        expected = "{}/compare.venn_src2.list".format(self.data_dir)
        observed = "venn_src2.list"
        self.assertTrue(self.are_same_files(expected, observed))
        expected = "{}/compare.all_sources_venn.list".format(self.data_dir)
        observed = "all_sources_venn.list"
        self.assertTrue(self.are_same_files(expected, observed))

        # check upsetplot
        expected = "{}/compare.upsetplot.png".format(self.data_dir)
        observed = "upsetplot.png"
        #self.assertTrue(self.__are_same_image(expected,observed))
        self.assertTrue(os.path.isfile(observed))

        # check gff specific files
        expected = "{}/compare.src1.specific_CDS_not_loci.gff".format(self.data_dir)
        observed = "src1.specific_CDS_not_loci.gff"
        self.assertTrue(self.are_same_files(expected, observed))
        expected = "{}/compare.src2.specific_CDS_not_loci.gff".format(self.data_dir)
        observed = "src2.specific_CDS_not_loci.gff"
        self.assertTrue(self.are_same_files(expected, observed))
        expected = "{}/compare.src1.specific_loci.gff".format(self.data_dir)
        observed = "src1.specific_loci.gff"
        self.assertTrue(self.are_same_files(expected, observed))
        expected = "{}/compare.src2.specific_loci.gff".format(self.data_dir)
        observed = "src2.specific_loci.gff"
        self.assertTrue(self.are_same_files(expected, observed))

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestCompare)
    unittest.TextTestRunner(verbosity=2).run(suite)
