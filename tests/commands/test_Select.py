#!/usr/bin/env python3
'''
Select command test class
'''
import unittest
import os
from tests.ingenannot_test import IngenannotTest
from ingenannot.commands.select import Select
from ingenannot.entities.transcript import Transcript
from ingenannot.main import ArgParse

class TestSelect(IngenannotTest):
    '''
    TestSelect
    '''

    def tearDown(self):
        try:
            os.remove("select.gff")
            os.remove("select.gff.scatter_hist_aed.png")
            os.remove("fof.cfg")
            os.remove("no-export.select.gff")
        except OSError:
            pass


    #@unittest.skip("pass")
    def test_run(self):
        '''test basic run'''

        # select noaed
        fof = self.write_fof(self.data_dir, [('select.CURTIN.gff','CURTIN'),
            ('select.eugene.gff','eugene')])
        output = "select.gff"
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output, "--noaed"])
        inst = Select(args)
        status = inst.run()
        expected = "{}/select.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

        # select no longreads, no penalty
        fof = self.write_fof(self.data_dir, [('select.CURTIN.gff','CURTIN'),
            ('select.eugene.gff','eugene')])
        output = "select.gff"
        evtr = "{}/evidence-rnaseq.gff.gz".format(self.data_dir)
        evpr = "{}/evidence-protein.gff.gz".format(self.data_dir)
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output, "--evtr", evtr, "--evpr", evpr,
            "--evtrstranded", "--evprstranded", "--clustranded"])
        inst = Select(args)
        status = inst.run()
        expected = "{}/select.2.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

        # select all with penalty == select noaed
        fof = self.write_fof(self.data_dir, [('select.CURTIN.gff','CURTIN'),
            ('select.eugene.gff','eugene')])
        output = "select.gff"
        evtr = "{}/evidence-rnaseq.gff.gz".format(self.data_dir)
        evpr = "{}/evidence-protein.gff.gz".format(self.data_dir)
        evlg = "{}/evidence-isoseq.gff.gz".format(self.data_dir)
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output, "--evtr", evtr, "--evpr", evpr,
            "--penalty_overflow", "0.25", "--longreads", evlg,
            "--evtrstranded", "--evprstranded", "--clustranded"])
        inst = Select(args)
        status = inst.run()
        expected = "{}/select.3.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

        # select all with penalty, with miniprot as protein source 
        fof = self.write_fof(self.data_dir, [('select.CURTIN.gff','CURTIN'),
            ('select.eugene.gff','eugene')])
        output = "select.gff"
        evtr = "{}/evidence-rnaseq.gff.gz".format(self.data_dir)
        evpr = "{}/evidence-protein.miniprot.gff.gz".format(self.data_dir)
        evlg = "{}/evidence-isoseq.gff.gz".format(self.data_dir)
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output, "--evtr", evtr, "--evpr", evpr,
            "--penalty_overflow", "0.25", "--longreads", evlg,
            "--evtrstranded", "--evprstranded", "--clustranded",
            "--evpr_source", "miniprot"])
        inst = Select(args)
        status = inst.run()
        expected = "{}/select.4.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)


    #@unittest.skip("pass")
    def test_run_multi_cds_overlap(self):
        '''multi CDS'''

        # select noaed       
        fof = self.write_fof(self.data_dir, [
        ("select.overlap.CURTIN.gff","CURTIN"),
        ("select.overlap.MPI.gff","MPI"),
        ("select.overlap.eugene.gff","eugene"),
        ("select.overlap.gmove_isoseq.gff","gmove_isoseq"),
        ("select.overlap.PASA-MPI.gff","PASA-MPI"),
        ("select.overlap.JGI.gff","JGI"),
        ("select.overlap.gmove_rnaseq_no_zymo.gff","gmove_rnaseq_no_zymo"),
        ("select.overlap.lorean.gff","lorean"),
        ("select.overlap.RRES.gff","RRES")])
        output = "select.gff"
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output, "--noaed", "--evtrstranded",
            "--evprstranded", "--clustranded", "--aedtr","0.2","--aedpr",
            "0.1","--nbsrc_filter", "5","--use_ev_lg"])
        inst = Select(args)
        status = inst.run()
        expected = "{}/select.overlap.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)


    #@unittest.skip("pass")
    def test_run_compute_aed_tr_on_cds(self):
        '''test aed on tr and cds and small CDS included'''

        fof = self.write_fof(self.data_dir, [
        ("select.computeaed.CURTIN.gff","CURTIN"),
        ("select.computeaed.MPI.gff","MPI"),
        ("select.computeaed.eugene.gff","eugene"),
        ("select.computeaed.PASA-MPI.gff","PASA-MPI"),
        ("select.computeaed.JGI.gff","JGI"),
        ("select.computeaed.gmove_rnaseq_no_zymo.gff","gmove_rnaseq_no_zymo"),
        ("select.computeaed.lorean.gff","lorean"),
        ("select.computeaed.RRES.gff","RRES")])
        output = "select.gff"
        evtr = "{}/select.cds-evidence-rnaseq.gff.gz".format(self.data_dir)
        evpr = "{}/select.cds-evidence-protein.gff.gz".format(self.data_dir)
        evlg = "{}/select.cds-evidence-isoseq.gff.gz".format(self.data_dir)
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output, "--evtr", evtr, "--evpr", evpr,
            "--penalty_overflow", "0.25", "--evtrstranded", "--evprstranded",
            "--clustranded", "--aedtr","0.27","--aedpr","0.1",
            "--nbsrc_filter", "5","--use_ev_lg"])
        inst = Select(args)
        status = inst.run()
        expected = "{}/select.computeaed.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

        args = sp.parse_args([fof,output, "--evtr", evtr, "--evpr", evpr,
            "--penalty_overflow", "0.25", "--evtrstranded", "--evprstranded",
            "--clustranded", "--aedtr","0.2","--aedpr","0.1",
            "--nbsrc_filter", "5","--use_ev_lg", "--aed_tr_cds_only"])
        inst = Select(args)
        status = inst.run()
        expected = "{}/select.computeaed.cds.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

    #@unittest.skip("pass")
    def test_run_nopartial_nb_sources(self):
        '''small CDS included'''

        fof = self.write_fof(self.data_dir, [
        ("select.computeaed.CURTIN.gff","CURTIN"),
        ("select.computeaed.MPI.gff","MPI"),
        ("select.computeaed.eugene.gff","eugene"),
        ("select.computeaed.PASA-MPI.gff","PASA-MPI"),
        ("select.computeaed.JGI.gff","JGI"),
        ("select.computeaed.gmove_rnaseq_no_zymo.gff","gmove_rnaseq_no_zymo"),
        ("select.computeaed.lorean.gff","lorean"),
        ("select.computeaed.RRES.gff","RRES")])
        output = "select.gff"
        evtr = "{}/select.cds-evidence-rnaseq.gff.gz".format(self.data_dir)
        evpr = "{}/select.cds-evidence-protein.gff.gz".format(self.data_dir)
        evlg = "{}/select.cds-evidence-isoseq.gff.gz".format(self.data_dir)
        genome = "{}/chr1.fasta".format(self.data_dir)
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output, "--evtr", evtr, "--evpr", evpr,
            "--penalty_overflow", "0.25", "--evtrstranded", "--evprstranded",
            "--clustranded", "--aedtr","0.27","--aedpr","0.1",
            "--nbsrc_filter", "5","--use_ev_lg", "--no_partial", "--genome",
            genome, "--nbsrc_absolute", "3", "--no_cds_overlap"])
        inst = Select(args)
        status = inst.run()
        expected = "{}/select.computeaed.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)


    #@unittest.skip("pass")
    def test_cds_overlap_rescue(self):
        '''pass'''

        fof = self.write_fof(self.data_dir, [
        ("select.cdsoverlap.CURTIN.gff","CURTIN"),
        ("select.cdsoverlap.MPI.gff","MPI"),
        ("select.cdsoverlap.eugene.gff","eugene"),
        ("select.cdsoverlap.JGI.gff","JGI"),
        ("select.cdsoverlap.RRES.gff","RRES")])
        output = "select.gff"
        evtr = "{}/select.cds-evidence-rnaseq.gff.gz".format(self.data_dir)
        evpr = "{}/select.cds-evidence-protein.gff.gz".format(self.data_dir)
        evlg = "{}/select.cds-evidence-isoseq.gff.gz".format(self.data_dir)
        genome = "{}/chr1.fasta".format(self.data_dir)
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output, "--noaed", "--clustranded",
            "--no_partial", "--genome", genome, "--use_ev_lg",
            "--no_cds_overlap", "--nbsrc_filter", "4",  "--aedtr", "0.4",
            "--aedpr", "0.1"])

        inst = Select(args)
        status = inst.run()
        expected = "{}/select.cdsoverlap.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

    #@unittest.skip("pass")
    def test_cds_overlap_rescue_2(self):
        '''pass'''

        fof = self.write_fof(self.data_dir, [
        ("select.cdsoverlap.CURTIN.2.gff","CURTIN"),
        ("select.cdsoverlap.MPI.2.gff","MPI"),
        ("select.cdsoverlap.eugene.2.gff","eugene"),
        ("select.cdsoverlap.JGI.2.gff","JGI"),
        ("select.cdsoverlap.RRES.2.gff","RRES")])
        output = "select.gff"
        evtr = "{}/select.cds-evidence-rnaseq.gff.gz".format(self.data_dir)
        evpr = "{}/select.cds-evidence-protein.gff.gz".format(self.data_dir)
        evlg = "{}/select.cds-evidence-isoseq.gff.gz".format(self.data_dir)
        genome = "{}/chr1.fasta".format(self.data_dir)
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output, "--noaed", "--clustranded",
            "--no_partial", "--genome", genome, "--use_ev_lg",
            "--no_cds_overlap", "--nbsrc_filter", "4",  "--aedtr", "0.4",
            "--aedpr", "0.1"])

        inst = Select(args)
        status = inst.run()
        expected = "{}/select.cdsoverlap.2.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

    #@unittest.skip("pass")
    def test_cds_overlap_rescue_3(self):
        '''pass'''

        fof = self.write_fof(self.data_dir, [
        ("select.cdsoverlap.CURTIN.3.gff","CURTIN"),
        ("select.cdsoverlap.MPI.3.gff","MPI"),
        ("select.cdsoverlap.eugene.3.gff","eugene"),
        ("select.cdsoverlap.JGI.3.gff","JGI"),
        ("select.cdsoverlap.RRES.3.gff","RRES")])
        output = "select.gff"
        evtr = "{}/select.cds-evidence-rnaseq.gff.gz".format(self.data_dir)
        evpr = "{}/select.cds-evidence-protein.gff.gz".format(self.data_dir)
        evlg = "{}/select.cds-evidence-isoseq.gff.gz".format(self.data_dir)
        genome = "{}/chr1.fasta".format(self.data_dir)
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output, "--noaed", "--clustranded",
            "--no_partial", "--genome", genome, "--use_ev_lg", "--no_cds_overlap",
            "--nbsrc_filter", "4",  "--aedtr", "0.4", "--aedpr", "0.1"])

        inst = Select(args)
        status = inst.run()
        expected = "{}/select.cdsoverlap.3.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        self.assertEqual(status, 0)

    #@unittest.skip("pass")
    def test_cds_overlap_rescue_4(self):
        '''pass'''

        fof = self.write_fof(self.data_dir, [
        ("select.cdsoverlap.CURTIN.4.gff","CURTIN"),
        ("select.cdsoverlap.MPI.4.gff","MPI"),
        ("select.cdsoverlap.eugene.4.gff","eugene"),
        ("select.cdsoverlap.JGI.4.gff","JGI"),
        ("select.cdsoverlap.RRES.4.gff","RRES")])
        output = "select.gff"
        noexport = "no-export.select.gff"
        evtr = "{}/select.cds-evidence-rnaseq.gff.gz".format(self.data_dir)
        evpr = "{}/select.cds-evidence-protein.gff.gz".format(self.data_dir)
        evlg = "{}/select.cds-evidence-isoseq.gff.gz".format(self.data_dir)
        genome = "{}/chr1.fasta".format(self.data_dir)
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output, "--noaed", "--clustranded",
            "--no_partial", "--genome", genome, "--use_ev_lg",
            "--no_cds_overlap", "--nbsrc_filter", "4",  "--aedtr", "0.4",
            "--aedpr", "0.1", "--no_export"])

        inst = Select(args)
        status = inst.run()
        expected = "{}/select.cdsoverlap.4.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, observed))
        expected = "{}/select.cdsoverlap.4.noexport.gff".format(self.data_dir)
        observed = noexport
        self.assertTrue(self.are_same_files(expected, noexport))
        self.assertEqual(status, 0)


    #@unittest.skip("pass")
    def test_filtering(self):
        '''pass'''

        fof = self.write_fof(self.data_dir, [
        ("select.cdsoverlap.CURTIN.4.gff","CURTIN", "CURTIN_gaeval.tsv"),
        ("select.cdsoverlap.MPI.4.gff","MPI", "MPI_gaeval.tsv"),
        ("select.cdsoverlap.eugene.4.gff","eugene", "eugene_gaeval.tsv"),
        ("select.cdsoverlap.JGI.4.gff","JGI", "JGI_gaeval.tsv"),
        ("select.cdsoverlap.RRES.4.gff","RRES", "RRES_gaeval.tsv")])
        output = "select.gff"
        genome = "{}/chr1.fasta".format(self.data_dir)
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output, "--noaed", "--clustranded",
            "--no_partial", "--genome", genome, "--use_ev_lg",
            "--no_cds_overlap", "--nbsrc_filter", "5",  "--aedtr",
            "0.5", "--aedpr", "0.1", "--gaeval"])

        inst = Select(args)
        status = inst.run()
        expected = "{}/select.filtering.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, output))
        self.assertEqual(status, 0)

    #@unittest.skip("pass")
    def test_gaeval(self):
        '''pass'''

        fof = self.write_fof(self.data_dir, [
        ("select.MPI.gaeval.gff","MPI", "select.MPI.gaeval.txt"),
        ("select.JGI.gaeval.gff","JGI", "select.JGI.gaeval.txt")])
        output = "select.gff"
        genome = "{}/chr1.fasta".format(self.data_dir)
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output, "--noaed", "--clustranded",
            "--no_partial", "--genome", genome, "--use_ev_lg",
            "--no_cds_overlap", "--nbsrc_filter", "5",  "--aedtr",
            "0.5", "--aedpr", "0.1", "--gaeval"])

        inst = Select(args)
        status = inst.run()
        expected = "{}/select.gaeval.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, output))
        self.assertEqual(status, 0)


    #@unittest.skip("pass")
    def test_export_notr(self):
        '''pass'''

        fof = self.write_fof(self.data_dir, [
        ("select.MPI.export.gff","MPI")])
        output = "select.gff"
        evtr = "{}/select.cds-evidence-rnaseq.gff.gz".format(self.data_dir)
        evpr = "{}/select.cds-evidence-protein.gff.gz".format(self.data_dir)
        evlg = "{}/select.cds-evidence-isoseq.gff.gz".format(self.data_dir)
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output, "--evtr", evtr, "--evpr", evpr,
            "--penalty_overflow", "0.25", "--evtrstranded", "--evprstranded",
            "--clustranded", "--use_ev_lg", "--aedpr", "0.1"])
        inst = Select(args)
        status = inst.run()
        expected = "{}/select.export.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, output))
        self.assertEqual(status, 0)


    #@unittest.skip("pass")
    def test_missing_genome_exception(self):
        '''missing genome'''

        fof = self.write_fof(self.data_dir, [('select.CURTIN.gff','CURTIN'),
            ('select.eugene.gff','eugene')])
        output = "select.gff"
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output, "--noaed", "--no_partial"])
        self.assertRaises(Exception,Select,args)

    #@unittest.skip("pass")
    def test_missing_evidences_files(self):
        '''missing evidences'''

        fof = self.write_fof(self.data_dir, [('select.CURTIN.gff','CURTIN'),
            ('select.eugene.gff','eugene')])
        output = "select.gff"
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output])
        inst = Select(args)
        self.assertRaises(Exception,inst.run)
        args = sp.parse_args([fof,output, "--evtr", "file"])
        inst = Select(args)
        self.assertRaises(Exception,inst.run)

    #@unittest.skip("pass")
    def test_get_aed(self):
        '''pass'''

        fof = self.write_fof(self.data_dir, [
        ("select.MPI.getaed.gff","MPI")])
        output = "select.gff"
        parser = ArgParse()
        sp = parser.subparser_select()
        args = sp.parse_args([fof,output, "--noaed", "--clustranded",
            "--use_ev_lg", "--no_cds_overlap", "--nbsrc_filter", "5",
            "--aedtr", "0.5", "--aedpr", "0.1"])

        inst = Select(args)
        status = inst.run()
        expected = "{}/select.getaed.gff".format(self.data_dir)
        observed = output
        self.assertTrue(self.are_same_files(expected, output))
        self.assertEqual(status, 0)

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestSelect)
    unittest.TextTestRunner(verbosity=2).run(suite)
