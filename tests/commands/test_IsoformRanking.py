#!/usr/bin/env python3
'''
isoform_ranking command test class
'''
import unittest
import os
import multiprocessing
from ingenannot.main import ArgParse
from ingenannot.commands.command import Command
from ingenannot.commands.isoform_ranking import IsoformRanking
from tests.ingenannot_test import IngenannotTest

class TestIsoformRanking(IngenannotTest):
    '''
    TestIsoformRanking
    '''

    def tearDown(self):

        if os.path.isfile("fof.cfg"):
            os.remove("fof.cfg")
        if os.path.isfile("isoforms.ranking.gff"):
            os.remove("isoforms.ranking.gff")
        if os.path.isfile("isoforms.unclassif.gff"):
            os.remove("isoforms.unclassif.gff")
        if os.path.isfile("isoforms.top.gff"):
            os.remove("isoforms.top.gff")
        if os.path.isfile("isoforms.alternatives.gff"):
            os.remove("isoforms.alternatives.gff")

    #@unittest.skip("skip")
    def test_get_bam_files_to_analyze(self):
        '''test_get_bam_files_to_analyze'''

        gff_tr = "{}/isorank.PB.isoform.ranking.gff".format(self.data_dir)
        bam = "{}/{}".format(self.data_dir,'isorank.SCA3419A31.subset.bam')
        bam2 = "{}/{}".format(self.data_dir,'isorank.SRR6215485.subset.bam')
        parser = ArgParse()
        sp = parser.subparser_isoform_ranking()
        args = sp.parse_args([gff_tr, "-b", bam])
        inst = IsoformRanking(args)
        bam_files = inst.get_bam_files_to_analyze()
        self.assertListEqual([(bam,False,False)],bam_files)

        fof = self.write_fof_isoform(self.data_dir, [('isorank.SCA3419A31.subset.bam',True,True),
            ('isorank.SRR6215485.subset.bam',False,True)])
        args = sp.parse_args([gff_tr, "-f", fof])
        inst = IsoformRanking(args)
        bam_files = inst.get_bam_files_to_analyze()
        self.assertListEqual([(bam,True,True),(bam2,False,True)],bam_files)

        fof = self.write_fof_isoform(self.data_dir, [('mock.subset.bam',True,True)])
        args = sp.parse_args([gff_tr, "-f", fof])
        inst = IsoformRanking(args)
        self.assertRaises(Exception,inst.get_bam_files_to_analyze)

        fof = self.write_fof_isoform(self.data_dir, [('isorank.mock.bam',True,True)])
        args = sp.parse_args([gff_tr, "-f", fof])
        inst = IsoformRanking(args)
        self.assertRaises(Exception,inst.get_bam_files_to_analyze)

        bam = "{}/{}".format(self.data_dir,'mock.subset.bam')
        args = sp.parse_args([gff_tr, "-b", bam])
        inst = IsoformRanking(args)
        self.assertRaises(Exception,inst.get_bam_files_to_analyze)

        bam = "{}/{}".format(self.data_dir,'isorank.mock.bam')
        args = sp.parse_args([gff_tr, "-b", bam])
        inst = IsoformRanking(args)
        self.assertRaises(Exception,inst.get_bam_files_to_analyze)

        args = sp.parse_args([gff_tr])
        inst = IsoformRanking(args)
        self.assertRaises(Exception,inst.get_bam_files_to_analyze)

    #@unittest.skip("skip")
    def test_run_1(self):
        '''test_run_1'''

        gff_tr = "{}/isorank.PB.isoform.ranking.gff".format(self.data_dir)
        bam = "{}/{}".format(self.data_dir,'isorank.SCA3419A31.subset.bam')
        sj_threshold = str(0.05)
        parser = ArgParse()
        sp = parser.subparser_isoform_ranking()
        args = sp.parse_args([gff_tr, "-b", bam, "--sj_threshold",
            sj_threshold, "--paired", "--stranded"])
        inst = IsoformRanking(args)
        status = inst.run()
        expected_rank = "{}/isorank.run1_1_isoforms.ranking.gff".format(self.data_dir)
        expected_top = "{}/isorank.run1_isoforms.top.gff".format(self.data_dir)
        expected_unclassif = "{}/isorank.run1_isoforms.unclassif.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertTrue(self.are_same_files(expected_top,"isoforms.top.gff"))
        self.assertTrue(self.are_same_files(expected_unclassif,"isoforms.unclassif.gff"))
        self.assertEqual(status, 0)

        sj_threshold = str(0.0)
        args = sp.parse_args([gff_tr, "-b", bam, "--sj_threshold",sj_threshold,
            "--paired", "--stranded"])
        inst.__init__(args)
        status = inst.run()
        expected_rank = "{}/isorank.run1_2_isoforms.ranking.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertEqual(status, 0)

        sj_threshold = str(0.05)
        args = sp.parse_args([gff_tr, "-b", bam, "--sj_threshold",sj_threshold,
            "--paired", "--stranded", "--sj_full"])
        inst.__init__(args)
        status = inst.run()
        expected_rank = "{}/isorank.run1_3_isoforms.ranking.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertEqual(status, 0)

        sj_threshold = str(0.0)
        args = sp.parse_args([gff_tr, "-b", bam, "--sj_threshold",sj_threshold,
            "--paired", "--stranded", "--sj_full"])
        inst.__init__(args)
        status = inst.run()
        expected_rank = "{}/isorank.run1_4_isoforms.ranking.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertEqual(status, 0)

        sj_threshold = str(0.05)
        args = sp.parse_args([gff_tr, "-b", bam, "--sj_threshold",sj_threshold,
            "--paired"])
        inst.__init__(args)
        status = inst.run()
        expected_rank = "{}/isorank.run1_1_isoforms.ranking.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertEqual(status, 0)

        sj_threshold = str(0.0)
        gff_tr = "{}/isorank.PB.2.isoform.ranking.gff".format(self.data_dir)
        args = sp.parse_args([gff_tr, "-b", bam, "--sj_threshold",sj_threshold,
            "--paired", "--stranded"])
        inst.__init__(args)
        status = inst.run()
        expected_rank = "{}/isorank.run1_5_isoforms.ranking.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertEqual(status, 0)

    #@unittest.skip("skip")
    def test_run_2(self):
        '''test_run_2'''

        gff_tr = "{}/isorank.PB.isoform.ranking.gff".format(self.data_dir)
        bam = "{}/{}".format(self.data_dir,'isorank.SRR6215485.subset.bam')
        sj_threshold = str(0.05)
        parser = ArgParse()
        sp = parser.subparser_isoform_ranking()
        args = sp.parse_args([gff_tr, "-b", bam, "--sj_threshold",sj_threshold,
            "--stranded"])
        inst = IsoformRanking(args)
        status = inst.run()
        expected_rank = "{}/isorank.run2_1_isoforms.ranking.gff".format(self.data_dir)
        expected_top = "{}/isorank.run2_1_isoforms.top.gff".format(self.data_dir)
        expected_unclassif = "{}/isorank.run2_1_isoforms.unclassif.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertTrue(self.are_same_files(expected_top,"isoforms.top.gff"))
        self.assertTrue(self.are_same_files(expected_unclassif,"isoforms.unclassif.gff"))
        self.assertEqual(status, 0)

        args = sp.parse_args([gff_tr, "-b", bam, "--sj_threshold",sj_threshold])
        inst = IsoformRanking(args)
        status = inst.run()
        expected_rank = "{}/isorank.run2_2_isoforms.ranking.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertEqual(status, 0)

    #@unittest.skip("skip")
    def test_run_3(self):
        '''test_run_3'''

        gff_tr = "{}/isorank.PB.isoform.ranking.gff".format(self.data_dir)
        bam = "{}/{}".format(self.data_dir,'isorank.SCA3419A31.subset.bam')
        sj_threshold = str(0.05)
        parser = ArgParse()
        sp = parser.subparser_isoform_ranking()
        args = sp.parse_args([gff_tr, "-b", bam, "--sj_threshold",sj_threshold,
            "--paired", "--stranded", "--rescue"])
        inst = IsoformRanking(args)
        status = inst.run()
        expected_rank = "{}/isorank.run3_isoforms.ranking.gff".format(self.data_dir)
        expected_top = "{}/isorank.run3_isoforms.top.gff".format(self.data_dir)
        expected_unclassif = "{}/isorank.run3_isoforms.unclassif.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertTrue(self.are_same_files(expected_top,"isoforms.top.gff"))
        self.assertTrue(self.are_same_files(expected_unclassif,"isoforms.unclassif.gff"))
        self.assertEqual(status, 0)

    #@unittest.skip("skip")
    def test_run_4(self):
        '''test_run_4'''

        gff_tr = "{}/isorank.PB.isoform.ranking.gff".format(self.data_dir)
        fof = self.write_fof_isoform(self.data_dir, [('isorank.SCA3419A31.subset.bam',True,True),
            ('isorank.SRR6215485.subset.bam',False,True)])
        sj_threshold = str(0.05)
        parser = ArgParse()
        sp = parser.subparser_isoform_ranking()
        args = sp.parse_args([gff_tr, "-f", fof, "--sj_threshold",sj_threshold])
        inst = IsoformRanking(args)
        status = inst.run()
        expected_rank = "{}/isorank.run4_isoforms.ranking.gff".format(self.data_dir)
        expected_top = "{}/isorank.run4_isoforms.top.gff".format(self.data_dir)
        expected_unclassif = "{}/isorank.run4_isoforms.unclassif.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertTrue(self.are_same_files(expected_top,"isoforms.top.gff"))
        self.assertTrue(self.are_same_files(expected_unclassif,"isoforms.unclassif.gff"))
        self.assertEqual(status, 0)

    #@unittest.skip("skip")
    def test_run_5(self):
        '''test add new isoform supported by one sample'''

        gff_tr = "{}/isorank.PB.3.isoform.ranking.gff".format(self.data_dir)
        fof = self.write_fof_isoform(self.data_dir, [('isorank.SCA3419A31.subset.2.bam',True,True),
            ('isorank.SRR6215485.subset.2.bam',False,True)])
        sj_threshold = str(0.0)
        parser = ArgParse()
        sp = parser.subparser_isoform_ranking()
        args = sp.parse_args([gff_tr, "-f", fof, "--sj_threshold",sj_threshold])
        inst = IsoformRanking(args)
        status = inst.run()
        expected_rank = "{}/isorank.run5_1_isoforms.ranking.gff".format(self.data_dir)
        expected_top = "{}/isorank.run5_1_isoforms.top.gff".format(self.data_dir)
        expected_unclassif = "{}/isorank.run5_1_isoforms.unclassif.gff".format(self.data_dir)
        expected_alternatives = "{}/isorank.run5_1_isoforms.alternatives.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertTrue(self.are_same_files(expected_top,"isoforms.top.gff"))
        self.assertTrue(self.are_same_files(expected_unclassif,"isoforms.unclassif.gff"))
        self.assertTrue(self.are_same_files(expected_alternatives,"isoforms.alternatives.gff"))
        self.assertEqual(status, 0)

        fof = self.write_fof_isoform(self.data_dir, [('isorank.SCA3419A31.subset.2.bam',True,True),
            ('isorank.test.bam',False,True)])
        args = sp.parse_args([gff_tr, "-f", fof, "--sj_threshold",sj_threshold])
        inst = IsoformRanking(args)
        status = inst.run()
        expected_rank = "{}/isorank.run5_1_isoforms.ranking.gff".format(self.data_dir)
        expected_top = "{}/isorank.run5_1_isoforms.top.gff".format(self.data_dir)
        expected_unclassif = "{}/isorank.run5_1_isoforms.unclassif.gff".format(self.data_dir)
        expected_alternatives = "{}/isorank.run5_1_isoforms.alternatives.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertTrue(self.are_same_files(expected_top,"isoforms.top.gff"))
        self.assertTrue(self.are_same_files(expected_unclassif,"isoforms.unclassif.gff"))
        self.assertTrue(self.are_same_files(expected_alternatives,"isoforms.alternatives.gff"))
        self.assertEqual(status, 0)

        fof = self.write_fof_isoform(self.data_dir, [('isorank.SCA3419A31.subset.2.bam',True,True),
            ('isorank.test.bam',False,True)])
        args = sp.parse_args([gff_tr, "-f", fof, "--sj_threshold",sj_threshold,
            "--alt_threshold", 0.0])
        inst = IsoformRanking(args)
        status = inst.run()
        expected_rank = "{}/isorank.run5_1_isoforms.ranking.gff".format(self.data_dir)
        expected_top = "{}/isorank.run5_1_isoforms.top.gff".format(self.data_dir)
        expected_unclassif = "{}/isorank.run5_1_isoforms.unclassif.gff".format(self.data_dir)
        expected_alternatives = "{}/isorank.run5_2_isoforms.alternatives.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertTrue(self.are_same_files(expected_top,"isoforms.top.gff"))
        self.assertTrue(self.are_same_files(expected_unclassif,"isoforms.unclassif.gff"))
        self.assertTrue(self.are_same_files(expected_alternatives,"isoforms.alternatives.gff"))
        self.assertEqual(status, 0)

        fof = self.write_fof_isoform(self.data_dir, [('isorank.test.bam',False,True)])
        args = sp.parse_args([gff_tr, "-f", fof, "--sj_threshold",sj_threshold])
        inst = IsoformRanking(args)
        status = inst.run()
        expected_rank = "{}/isorank.run5_3_isoforms.ranking.gff".format(self.data_dir)
        expected_top = "{}/isorank.run5_3_isoforms.top.gff".format(self.data_dir)
        expected_unclassif = "{}/isorank.run5_3_isoforms.unclassif.gff".format(self.data_dir)
        expected_alternatives = "{}/isorank.run5_3_isoforms.alternatives.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertTrue(self.are_same_files(expected_top,"isoforms.top.gff"))
        self.assertTrue(self.are_same_files(expected_unclassif,"isoforms.unclassif.gff"))
        self.assertTrue(self.are_same_files(expected_alternatives,"isoforms.alternatives.gff"))
        self.assertEqual(status, 0)

        fof = self.write_fof_isoform(self.data_dir, [('isorank.test.bam',False,True)])
        args = sp.parse_args([gff_tr, "-f", fof, "--sj_threshold",sj_threshold,
            "--alt_threshold", 0.0])
        inst = IsoformRanking(args)
        status = inst.run()
        expected_rank = "{}/isorank.run5_3_isoforms.ranking.gff".format(self.data_dir)
        expected_top = "{}/isorank.run5_3_isoforms.top.gff".format(self.data_dir)
        expected_unclassif = "{}/isorank.run5_3_isoforms.unclassif.gff".format(self.data_dir)
        expected_alternatives = "{}/isorank.run5_4_isoforms.alternatives.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertTrue(self.are_same_files(expected_top,"isoforms.top.gff"))
        self.assertTrue(self.are_same_files(expected_unclassif,"isoforms.unclassif.gff"))
        self.assertTrue(self.are_same_files(expected_alternatives,"isoforms.alternatives.gff"))
        self.assertEqual(status, 0)


    #@unittest.skip("skip")
    def test_run_6(self):
        '''test add new isoform supported by one sample'''

        gff_tr = "{}/isorank.tr.isoform.ranking.gff".format(self.data_dir)
        fof = self.write_fof_isoform(self.data_dir, [('isorank.SCA3419A40.subset.bam',True,True)])
#            ('isorank.SRR6215485.subset.2.bam',False,True)])
        sj_threshold = str(0.10)
        parser = ArgParse()
        sp = parser.subparser_isoform_ranking()
        args = sp.parse_args([gff_tr, "-f", fof, "--sj_threshold",sj_threshold])
        inst = IsoformRanking(args)
        status = inst.run()
        expected_rank = "{}/isorank.run6_1_isoforms.ranking.gff".format(self.data_dir)
        expected_top = "{}/isorank.run6_1_isoforms.top.gff".format(self.data_dir)
        expected_unclassif = "{}/isorank.run6_1_isoforms.unclassif.gff".format(self.data_dir)
        expected_alternatives = "{}/isorank.run6_1_isoforms.alternatives.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertTrue(self.are_same_files(expected_top,"isoforms.top.gff"))
        self.assertTrue(self.are_same_files(expected_unclassif,"isoforms.unclassif.gff"))
        self.assertTrue(self.are_same_files(expected_alternatives,"isoforms.alternatives.gff"))
        self.assertEqual(status, 0)

    @unittest.skip("skip")
    def test_run_multi_proc_1(self):
        '''test_run_multi_proc_1'''

        gff_tr = "{}/isorank.PB.isoform.ranking.gff".format(self.data_dir)
        bam = "{}/{}".format(self.data_dir,'isorank.SCA3419A31.subset.bam')
        sj_threshold = str(0.05)
        parser = ArgParse()
        sp = parser.subparser_isoform_ranking()
        args = sp.parse_args([gff_tr, "-b", bam, "--sj_threshold",sj_threshold,
            "--paired", "--stranded"])
        procs = 4 
        Command.NB_CPUS = min(procs, multiprocessing.cpu_count()-1)
        inst = IsoformRanking(args)
        status = inst.run()
        expected_rank = "{}/isorank.run1_1_isoforms.ranking.gff".format(self.data_dir)
        expected_top = "{}/isorank.run1_isoforms.top.gff".format(self.data_dir)
        expected_unclassif = "{}/isorank.run1_isoforms.unclassif.gff".format(self.data_dir)
        self.assertTrue(self.are_same_files(expected_rank,"isoforms.ranking.gff"))
        self.assertTrue(self.are_same_files(expected_top,"isoforms.top.gff"))
        self.assertTrue(self.are_same_files(expected_unclassif,"isoforms.unclassif.gff"))
        self.assertEqual(status, 0)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestIsoformRanking)
    unittest.TextTestRunner(verbosity=2).run(suite)
