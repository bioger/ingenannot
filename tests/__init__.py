import os

def get_test_data_dir():

    return os.path.abspath("../test-data")
