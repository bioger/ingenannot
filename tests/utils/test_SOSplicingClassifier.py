#!/usr/bin/env python3

import unittest

from ingenannot.utils.so_splicing_classifier import SOSplicingClassifier
from ingenannot.entities.gene import Gene
from ingenannot.entities.transcript import Transcript
from ingenannot.entities.exon import Exon
from ingenannot.entities.cds import CDS


class TestSOSplicingClassifier(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_classify_gene(self):

        g1 = Gene('g1','chr1',5741639,5745251,-1)
        t1 = Transcript('t1','chr1',5741639,5744251,-1,'g1')
        g1.add_transcript(t1)
        t1.add_exon(Exon('ex1','chr1',5741639, 5742376,-1, []))
        t1.add_exon(Exon('ex2','chr1',5742668, 5743353,-1,[]))
        t1.add_exon(Exon('ex3','chr1',5743419, 5744251,-1,[]))

        expected_classification = (0,0,0)
        observed_classification = SOSplicingClassifier.classify_gene(g1)
        self.assertEqual(expected_classification, observed_classification)

        t2 = Transcript('t2','chr1',5741640,5744181,-1,'g1','rna-seq')
        g1.add_transcript(t2)
        t2.add_exon(Exon('exon:chr_1-5741640-5742376','chr1',5741640,5742376,-1, []))
        t2.add_exon(Exon('exon:chr_1-5742668-5743353','chr1',5742668,5743353,-1, []))
        t2.add_exon(Exon('exon:chr_1-5743419-5744181','chr1',5743419,5744181,-1, []))

        expected_classification = (0,0,1)
        observed_classification = SOSplicingClassifier.classify_gene(g1)
        self.assertEqual(expected_classification, observed_classification)

        t3 = Transcript('t3','chr1',5744282,5745251,-1,'g1','rna-seq')
        g1.add_transcript(t3)
        t3.add_exon(Exon('exon:t3.1','chr1',5744282,5744600,-1, []))
        t3.add_exon(Exon('exon:t3.2','chr1',5744650,5745251,-1, []))

        expected_classification = (2,0,1)
        observed_classification = SOSplicingClassifier.classify_gene(g1)
        self.assertEqual(expected_classification, observed_classification)

        t4 = Transcript('t4','chr1',5744282,5745251,-1,'g1','rna-seq')
        g1.add_transcript(t4)
        t4.add_exon(Exon('exon:t4.1','chr1',5744282,5744650,-1, []))
        t4.add_exon(Exon('exon:t4.2','chr1',5744680,5745251,-1, []))

        expected_classification = (4,1,1)
        observed_classification = SOSplicingClassifier.classify_gene(g1)
        self.assertEqual(expected_classification, observed_classification)

    def test_are_transcript_sequence_disjoint(self):

        t2 = Transcript('t2','chr1',5741640,5744181,-1,'g1','rna-seq')
        t2.add_exon(Exon('exon:chr_1-5741640-5742376','chr1',5741640,5742376,-1, []))
        t2.add_exon(Exon('exon:chr_1-5742668-5743353','chr1',5742668,5743353,-1, []))
        t2.add_exon(Exon('exon:chr_1-5743419-5744181','chr1',5743419,5744181,-1, []))

        t3 = Transcript('t3','chr1',5744282,5745251,-1,'g1','rna-seq')
        t3.add_exon(Exon('exon:t3.1','chr1',5744282,5744600,-1, []))
        t3.add_exon(Exon('exon:t3.2','chr1',5744650,5745251,-1, []))
        t3.add_cds(CDS('cds3','chr1',5744285, 5744500,-1, 0, 't3'))

        t4 = Transcript('t4','chr1',5744282,5745251,-1,'g1','rna-seq')
        t4.add_exon(Exon('exon:t4.1','chr1',5744282,5744650,-1, []))
        t4.add_exon(Exon('exon:t4.2','chr1',5744680,5745251,-1, []))
        t4.add_cds(CDS('cds4','chr1',5744690, 5745200,-1, 0, 't4'))

        self.assertFalse(SOSplicingClassifier.are_transcripts_sequence_disjoint(t3, t4, 'exon'))
        self.assertTrue(SOSplicingClassifier.are_transcripts_sequence_disjoint(t3, t4, 'cds'))
        self.assertTrue(SOSplicingClassifier.are_transcripts_sequence_disjoint(t3, t2, 'exon'))



if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestSOSplicingClassifier)
    unittest.TextTestRunner(verbosity=2).run(suite)

