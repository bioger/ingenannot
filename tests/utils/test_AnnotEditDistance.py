#!/usr/bin/env python3

import unittest
from ingenannot.utils.annot_edit_distance import AnnotEditDistance
from ingenannot.entities.gene import Gene
from ingenannot.entities.transcript import Transcript
from ingenannot.entities.exon import Exon
from ingenannot.entities.cds import CDS

class TestAnnotEditDistance(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass


    def test_incongruence(self):

        tr1 = Transcript('tr1','chr1',5741639,5744251,-1,'gene:gene:gene:chr_1g0027131-eugene_1.6.1_run1-ingenannot')
        tr1.add_exon(Exon('ex1','chr1',5741639, 5742376,-1, []))
        tr1.add_exon(Exon('ex2','chr1',5742668, 5743353,-1,[]))
        tr1.add_exon(Exon('ex3','chr1',5743419, 5744251,-1,[]))
        tr1.add_cds(CDS('cds1','chr1',5742296, 5742376,-1, 0, 'tr1'))
        tr1.add_cds(CDS('cds2','chr1',5742668, 5743353,-1, 2, 'tr1'))
        tr1.add_cds(CDS('cds3','chr1',5743419, 5743908,-1, 0, 'tr1'))

        tr2 = Transcript('tr2','chr1',5741640,5744181,-1,'SRR6215486.3361','rna-seq')
        tr2.add_exon(Exon('exon:chr_1-5741640-5742376','chr1',5741640,5742376,-1, []))
        tr2.add_exon(Exon('exon:chr_1-5742668-5743353','chr1',5742668,5743353,-1, []))
        tr2.add_exon(Exon('exon:chr_1-5743419-5744181','chr1',5743419,5744181,-1, []))

        expected_distance = "0.212489"
        observed_distance = "{:.6f}".format(AnnotEditDistance.incongruence(tr1, tr2, tr1_no_utr=True, tr2_no_utr=False))
        self.assertEqual(expected_distance, observed_distance)

        expected_distance = "0.015729"
        observed_distance = "{:.6f}".format(AnnotEditDistance.incongruence(tr1, tr2, tr1_no_utr=False, tr2_no_utr=False))
        self.assertEqual(expected_distance, observed_distance)

        tr3 = Transcript('tr3', 'chr1',5741647, 5744249, -1, 'gene:evm.TU.chr_1.2039.1-PASA_run1')
        tr3.add_exon(Exon('exon:evm.TU.chr_1.2039.1.1','chr1',5741647,5743353,-1,[]))
        tr3.add_exon(Exon('exon:evm.TU.chr_1.2039.1.2','chr1',5743419,5744249,-1,[]))
        tr3.add_cds(CDS('cds:evm.TU.chr_1.2039.1','chr1',5742653,5743353,-1,2,'tr3'))
        tr3.add_cds(CDS('cds:evm.TU.chr_1.2039.1','chr1',5743419,5743908,-1,0,'tr3'))

        tr4 = Transcript('tr4','chr1',5741647,5744249,-1,'rna-seq')
        tr4.add_exon(Exon('exon:chr_1-5741647-5743353','chr1',5741647,5743353,-1,[]))
        tr4.add_exon(Exon('exon:chr_1-5743419-5744249','chr1',5743419,5744249,-1,[]))

        expected_distance = "0.265366"
        observed_distance = "{:.6f}".format(AnnotEditDistance.incongruence(tr3, tr4, tr1_no_utr=True, tr2_no_utr=False))
        self.assertEqual(expected_distance, observed_distance)

        expected_distance = "0.000000"
        observed_distance = "{:.6f}".format(AnnotEditDistance.incongruence(tr3, tr4, tr1_no_utr=False, tr2_no_utr=False))
        self.assertEqual(expected_distance, observed_distance)

        tr5 = Transcript('tr5','chr2',5741647,5744249,-1,'rna-seq')
        tr5.add_exon(Exon('exon:chr_2-5741647-5743353','chr2',5741647,5743353,-1,[]))
        tr5.add_exon(Exon('exon:chr_2-5743419-5744249','chr2',5743419,5744249,-1,[]))

        expected_distance = "1.0"
        observed_distance = "{:.1f}".format(AnnotEditDistance.incongruence(tr4, tr5, tr1_no_utr=False, tr2_no_utr=False))
        self.assertEqual(expected_distance, observed_distance)



    def test_annot_edit_distance_between_2_gene_releases(self):

        g1 = Gene('g1','chr1',5741639,5744251,-1)
        tr1 = Transcript('tr1','chr1',5741639,5744251,-1,'g1')
        g1.add_transcript(tr1)
        tr1.add_exon(Exon('ex1','chr1',5741639, 5742376,-1, []))
        tr1.add_exon(Exon('ex2','chr1',5742668, 5743353,-1,[]))
        tr1.add_exon(Exon('ex3','chr1',5743419, 5744251,-1,[]))
        tr1.add_cds(CDS('cds1','chr1',5742296, 5742376,-1, 0, 'tr1'))
        tr1.add_cds(CDS('cds2','chr1',5742668, 5743353,-1, 2, 'tr1'))
        tr1.add_cds(CDS('cds3','chr1',5743419, 5743908,-1, 0, 'tr1'))

        g2 = Gene('g2','chr1',5741640,5744181,-1)
        tr2 = Transcript('tr2','chr1',5741640,5744181,-1,'g2','rna-seq')
        g2.add_transcript(tr2)
        tr2.add_exon(Exon('exon:chr_1-5741640-5742376','chr1',5741640,5742376,-1, []))
        tr2.add_exon(Exon('exon:chr_1-5742668-5743353','chr1',5742668,5743353,-1, []))
        tr2.add_exon(Exon('exon:chr_1-5743419-5744181','chr1',5743419,5744181,-1, []))

        expected_distance = "0.015729"
        observed_distance = "{:.6f}".format(AnnotEditDistance.annot_edit_distance_between_2_gene_releases(g1,g2))
        self.assertEqual(expected_distance, observed_distance)

        g3 = Gene('g3','chr2',5741640,5744181,-1)
        tr3 = Transcript('tr3', 'chr2',5741647, 5744249, -1, 'g3')
        tr3.add_exon(Exon('exon:evm.TU.chr_1.2039.1.1','chr2',5741647,5743353,-1,[]))
        tr3.add_exon(Exon('exon:evm.TU.chr_1.2039.1.2','chr2',5743419,5744249,-1,[]))
        tr3.add_cds(CDS('cds:evm.TU.chr_1.2039.1','chr2',5742653,5743353,-1,2,'tr3'))
        tr3.add_cds(CDS('cds:evm.TU.chr_1.2039.1','chr2',5743419,5743908,-1,0,'tr3'))

        expected_distance = "1.0"
        observed_distance = "{:.1f}".format(AnnotEditDistance.annot_edit_distance_between_2_gene_releases(g1,g3))
        self.assertEqual(expected_distance, observed_distance)

        g4 = Gene('g4','chr2',5741640,5744181,-1)
        expected_distance = "1.0"
        observed_distance = "{:.1f}".format(AnnotEditDistance.annot_edit_distance_between_2_gene_releases(g4,g4))
        self.assertEqual(expected_distance, observed_distance)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestAnnotEditDistance)
    unittest.TextTestRunner(verbosity=2).run(suite)

