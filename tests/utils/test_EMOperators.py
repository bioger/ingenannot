#!/usr/bin/env python3

import unittest

from ingenannot.utils.em_operators import EMOperators
from ingenannot.entities.gene import Gene
from ingenannot.entities.transcript import Transcript
from ingenannot.entities.exon import Exon
from ingenannot.entities.cds import CDS


class TestEMOperators(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_overlap(self):

        t1 = Transcript('t1','chr1',5741639,5744251,-1,'gene:gene:gene:chr_1g0027131-eugene_1.6.1_run1-ingenannot')
        t1.add_exon(Exon('ex1','chr1',5741639, 5742376,-1, []))
        t1.add_exon(Exon('ex2','chr1',5742668, 5743353,-1,[]))
        t1.add_exon(Exon('ex3','chr1',5743419, 5744251,-1,[]))
        t1.add_cds(CDS('cds1','chr1',5742296, 5742376,-1, 0, 't1'))
        t1.add_cds(CDS('cds2','chr1',5742668, 5743353,-1, 2, 't1'))
        t1.add_cds(CDS('cds3','chr1',5743419, 5743908,-1, 0, 't1'))

        t2 = Transcript('t2','chr1',5741640,5744181,-1,'SRR6215486.3361','rna-seq')
        t2.add_exon(Exon('exon:chr_1-5741640-5742376','chr1',5741640,5742376,-1, []))
        t2.add_exon(Exon('exon:chr_1-5742668-5743353','chr1',5742668,5743353,-1, []))
        t2.add_exon(Exon('exon:chr_1-5743419-5744181','chr1',5743419,5744181,-1, []))
        t2.add_cds(CDS('cds2','chr1',5742299, 5742379,-1, 0, 't1'))

        self.assertTrue(EMOperators.overlap(t1,t2, 'exon'))
        self.assertFalse(EMOperators.overlap(t1,t2, 'cds'))
        self.assertRaises(Exception, lambda:EMOperators.overlap(t1,t2, 'fake'))


    def test_disjoint(self):

        t1 = Transcript('t1','chr1',5741639,5744251,-1,'gene:gene:gene:chr_1g0027131-eugene_1.6.1_run1-ingenannot')
        t1.add_exon(Exon('ex1','chr1',5741639, 5742376,-1, []))
        t1.add_exon(Exon('ex2','chr1',5742668, 5743353,-1,[]))
        t1.add_exon(Exon('ex3','chr1',5743419, 5744251,-1,[]))
        t1.add_cds(CDS('cds1','chr1',5742296, 5742376,-1, 0, 't1'))
        t1.add_cds(CDS('cds2','chr1',5742668, 5743353,-1, 2, 't1'))
        t1.add_cds(CDS('cds3','chr1',5743419, 5743908,-1, 0, 't1'))

        t2 = Transcript('t2','chr1',5741640,5744181,-1,'SRR6215486.3361','rna-seq')
        t2.add_exon(Exon('exon:chr_1-5741640-5742376','chr1',5741640,5742376,-1, []))
        t2.add_exon(Exon('exon:chr_1-5742668-5743353','chr1',5742668,5743353,-1, []))
        t2.add_exon(Exon('exon:chr_1-5743419-5744181','chr1',5743419,5744181,-1, []))
        t2.add_cds(CDS('cds2','chr1',5742299, 5742379,-1, 0, 't1'))

        self.assertFalse(EMOperators.disjoint(t1,t2, 'exon'))
        self.assertTrue(EMOperators.disjoint(t1,t2, 'cds'))
        self.assertRaises(Exception, lambda:EMOperators.disjoint(t1,t2, 'fake'))

    def test_part_overlap(self):

        t1 = Transcript('t1','chr1',5741639,5744251,-1,'gene:gene:gene:chr_1g0027131-eugene_1.6.1_run1-ingenannot')
        t1.add_exon(Exon('ex1','chr1',5741639, 5742376,-1, []))
        t1.add_exon(Exon('ex2','chr1',5742668, 5743353,-1,[]))
        t1.add_exon(Exon('ex3','chr1',5743419, 5744251,-1,[]))
        t1.add_cds(CDS('cds1','chr1',5742296, 5742376,-1, 0, 't1'))
        t1.add_cds(CDS('cds2','chr1',5742668, 5743353,-1, 2, 't1'))
        t1.add_cds(CDS('cds3','chr1',5743419, 5743908,-1, 0, 't1'))

        t2 = Transcript('t2','chr1',5741640,5744181,-1,'SRR6215486.3361','rna-seq')
        t2.add_exon(Exon('exon:chr_1-5741640-5742376','chr1',5741640,5742376,-1, []))
        t2.add_exon(Exon('exon:chr_1-5742668-5743353','chr1',5742668,5743353,-1, []))
        t2.add_exon(Exon('exon:chr_1-5743419-5744181','chr1',5743419,5744181,-1, []))
        t2.add_cds(CDS('cds2','chr1',5742299, 5742379,-1, 0, 't1'))

        t3 = Transcript('t3', 'chr1',5741647, 5744249, -1, 'gene:evm.TU.chr_1.2039.1-PASA_run1')
        t3.add_exon(Exon('exon:evm.TU.chr_1.2039.1.1','chr1',5741647,5743353,-1,[]))
        t3.add_exon(Exon('exon:evm.TU.chr_1.2039.1.2','chr1',5743419,5744249,-1,[]))
        t3.add_cds(CDS('cds:evm.TU.chr_1.2039.1','chr1',5742653,5743353,-1,2,'t3'))
        t3.add_cds(CDS('cds:evm.TU.chr_1.2039.1','chr1',5743419,5743908,-1,0,'t3'))

        t4 = Transcript('t4','chr1',5741647,5744249,-1,'rna-seq')
        t4.add_exon(Exon('exon:chr_1-5741647-5743353','chr1',5741647,5743353,-1,[]))
        t4.add_exon(Exon('exon:chr_1-5743419-5744249','chr1',5743419,5744249,-1,[]))

        self.assertTrue(EMOperators.part_overlap(t1,t2, 'exon'))
        self.assertTrue(EMOperators.part_overlap(t1,t2, 'cds'))
        self.assertTrue(EMOperators.part_overlap(t1,t3, 'cds'))
        self.assertFalse(EMOperators.part_overlap(t1,t4, 'cds'))
        self.assertRaises(Exception, lambda:EMOperators.part_overlap(t1,t2, 'fake'))

    def test_binary_product(self):

        self.assertEqual(None,EMOperators.binary_product("foo","foo"))

    def test_difference(self):

        self.assertEqual(None,EMOperators.difference("foo","foo"))

    def test_binary_sum(self):

        self.assertEqual(None,EMOperators.binary_sum("foo","foo"))


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestEMOperators)
    unittest.TextTestRunner(verbosity=2).run(suite)

