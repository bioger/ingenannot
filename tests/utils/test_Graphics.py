#!/usr/bin/env python3

import sys
import os
import unittest
import numpy as np
import pandas as pd
from PIL import Image
from PIL import ImageChops

from ingenannot.utils.graphics import Graphics

class TestGraphics(unittest.TestCase):

    def setUp(self):

        self.data_dir = '{}/../../test-data'.format(os.path.abspath(os.path.dirname(__file__)))

    def tearDown(self):

        os.remove("out.png")

    def test_plot_aed_scatter_hist(self):
        """test_plot_aed_scatter_hist"""

        l_aed_tr = [0.1,0.2,0.6,0.5,0.2]
        l_aed_pr = [0.1,0.2,0.6,0.5,0.2]
        l_aed_tr_no_penalty = [0.6]
        l_aed_pr_no_penalty = [0.6]
        laed = [l_aed_tr, l_aed_pr, l_aed_tr_no_penalty, l_aed_pr_no_penalty]
        aedtr_filtering= 0.3
        aedpr_filtering = 0.1
        legend = ['aed_tr','aed_pr']
        title="test aed scatter hist"
        expected = "{}/plotAEDScatterHist.png".format(self.data_dir)
        observed = "out.png"
        Graphics.plot_aed_scatter_hist(laed,aedtr_filtering,aedpr_filtering,observed,legend,title)
        self.assertTrue(self.__are_same_image(expected,observed))

    def test_plot_distribution(self):
        """test_plot_distribution"""

        lXs = [1,2,3,4,5]
        lYs = [12,565,89,90,10]
        expected = "{}/plotDistribution.png".format(self.data_dir)
        observed = "out.png"
        Graphics.plot_distribution(lXs, lYs, out=observed, title="title",xax="xax",yax="yax", legend=["val"],grid=[6])
        self.assertTrue(self.__are_same_image(expected,observed))

    def test_plot_upsetplot(self):
        """test_plot_upsetplot"""

        sources = ["src1","src2","src3"]
        df = pd.DataFrame()
        df["src1"] = [True,True, True, True, True, True, True]
        df["src2"] = [True,False, True, True, True, False, False]
        df["src3"] = [True,True, False, True, True, False, False]
        df.reset_index(inplace=True)
        df.set_index(sources,inplace=True)
        expected = "{}/plotUpsetPlot.png".format(self.data_dir)
        observed = "out.png"
        Graphics.plot_upsetplot(df, out=observed, title="title")
        self.assertTrue(self.__are_same_image(expected,observed))

    def __are_same_image(self, exp, obs):

        img_exp = Image.open(exp)
        img_obs = Image.open(obs)
        return  np.array_equal(img_exp, img_obs)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestGraphics)
    unittest.TextTestRunner(verbosity=2).run(suite)


