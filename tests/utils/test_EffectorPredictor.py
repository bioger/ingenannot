#!/usr/bin/env python3

import unittest
import os
import logging
import shutil
from ingenannot.utils.effector_predictor import EffectorPredictor

class TestEffectorPredictor(unittest.TestCase):

    def setUp(self):

        self.data_dir = '{}/../../test-data'.format(os.path.abspath(os.path.dirname(__file__)))

    def tearDown(self):

        if os.path.isdir('effpred'):
            shutil.rmtree('effpred', ignore_errors=True)
        if os.path.isfile('out.txt'):
           os.remove("out.txt")
        if os.path.isfile('effectors.txt'):
           os.remove("effectors.txt")

    def test_run_class(self):

        #logging.getLogger().setLevel('DEBUG')
        fasta = "{}/effpred.proteins.fasta".format(self.data_dir)
        inst = EffectorPredictor(fasta)
        EffectorPredictor.EFFECTORP = "/usr/local/bin/EffectorP_2.0/Scripts/EffectorP.py"
        if inst.validate_tools_status == 0:
            df = inst.run()
            expected = ["cl_32_2","cl_35_4","cl_66_0"]
            observed = df.index.values.tolist()
            self.assertListEqual(expected,observed)
        else: # Exception tools
            self.assertRaises(Exception, inst.run)

    def test_run_validate_tools(self):

        fasta = "{}/effpred.proteins.fasta".format(self.data_dir)
        inst = EffectorPredictor(fasta)
        EffectorPredictor.SIGNALP = "/usr/fake"
        EffectorPredictor.SIGNALP_VERSION = 4.2
        EffectorPredictor.TMHMM = "/usr/local/bin/tmhmm-2.0c/bin/tmhmm-fake"
        EffectorPredictor.TARGETP = "/usr/local/bin/targetp_fake"
        EffectorPredictor.TARGETP_VERSION = "1.0b"
        EffectorPredictor.EFFECTORP = "/usr/local/bin/EffectorP_3.0/Scripts/EffectorP.py"
        EffectorPredictor.EFFECTORP_VERSION = "3.0"

        self.assertRaises(Exception,inst.run)
        EffectorPredictor.SIGNALP = "/usr/local/bin/signalp"
        self.assertRaises(Exception,inst.run)
        EffectorPredictor.SIGNALP_VERSION = 4.1
        self.assertRaises(Exception,inst.run)
        EffectorPredictor.TMHMM = "/usr/local/bin/tmhmm-2.0c/bin/tmhmm"
        self.assertRaises(Exception,inst.run)
        EffectorPredictor.TARGETP = "/usr/local/bin/targetp"
        self.assertRaises(Exception,inst.run)
        EffectorPredictor.TARGETP_VERSION = "1.1b"
        self.assertRaises(Exception,inst.run)
        EffectorPredictor.EFFECTORP = "/usr/local/bin/EffectorP_2.0/Scripts/EffectorP.py"
        self.assertRaises(Exception,inst.run)


    def __are_same_files(self, exp, obs):

        fh_exp = open(exp)
        fh_obs = open(obs)
        val = [row for row in fh_exp] == [row for row in fh_obs]
        fh_exp.close()
        fh_obs.close()
        return val


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestEffectorPredictor)
    unittest.TextTestRunner(verbosity=2).run(suite)

