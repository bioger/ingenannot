#!/usr/bin/env python3

import unittest
import numpy as np
from ingenannot.utils.statistics import Statistics

class TestStatistics(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass


    def test_geometric_median(self):

        points = np.array([[1,2,2,4,5],[5,4,2,2,1]])
        mean_geom = np.around(Statistics.geometric_median(points),2)
        l1 = list(mean_geom)
        l2 = [2.65, 2.65]
        self.assertListEqual(l1,l2) 

    def test_mean_median_distance(self):

        points = np.array([[1,2,2,4,5],[5,4,2,2,1]])
        mean_dist, median_dist = np.around(Statistics.mean_median_distance(points,[2.65,2.65]),2)
        self.assertEqual(1.93,mean_dist)
        self.assertEqual(1.5, median_dist)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestStatistics)
    unittest.TextTestRunner(verbosity=2).run(suite)
