#!/usr/bin/env python3

import unittest
import os

from ingenannot.utils.gff_reader import GFFReader, Feature
from ingenannot.utils.gene_builder import GeneBuilder, GFF3MiniprotBuilderFeature

class TestGeneBuilder(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    @unittest.skip("todo")
    def test_build_all_genes(self):
        pass
        #genes = builder.build_all_genes(features, coding_only=coding_only, source="test")

class TestGFF3MiniprotBuilderFeature(unittest.TestCase):

    def setUp(self):
        self.data_dir = '{}/../../test-data'.format(os.path.abspath(os.path.dirname(__file__)))

    def tearDown(self):
        pass

    def test_convert_features(self):

        gff_file = "{}/gene_builder.miniprot.gff".format(self.data_dir)
        reader = GFFReader(gff_file)
        features = [feat for feat in reader.read(downgraded=True)]
        builder = GFF3MiniprotBuilderFeature()
        new_features = builder.convert_features(features)
        f = Feature('MP000001','Chr2','miniprot','mRNA',13538012,13539220,920.0,-1,None,{'ID': ['MP000001'], 'Rank': ['1'], 'Identity': ['0.6838'], 'Positive': ['0.8419'], 'Target': ['101020_0:000000 47 294'], 'Parent': ['gene-MP000001']})
        self.assertEqual(new_features[0],f)
        f = Feature('exon-MP000001.4','Chr2','miniprot','exon',13538012,13538133,157.0,-1,2,{'Parent': ['MP000001'], 'Rank': ['1'], 'Identity': ['0.6750'], 'Target': ['101020_0:000000 254 294'], 'ID': ['exon-MP000001.4']})
        self.assertEqual(new_features[-1],f)

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromName('tests.utils.test_GeneBuilder')
    unittest.TextTestRunner(verbosity=2).run(suite)

