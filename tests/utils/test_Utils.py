#!/usr/bin/env python3

import unittest
import os
from tests.ingenannot_test import IngenannotTest
from ingenannot.utils import Utils
from ingenannot.entities.transcript import Transcript

class TestUtils(IngenannotTest):

    def tearDown(self):
        pass

    def test_get_sources_from_fof(self):
        pass

    def test_extract_genes_from_fof(self):
        pass

    def test_extract_genes(self):
        pass

    def test_clusterize(self):
        pass

    def test_get_metagenes_from_clusters(self):
        pass

    @unittest.skip("pass")
    def test_get_seq_length_from_fasta(self):

        fasta = "{}/utils.genome.fasta".format(self.data_dir)
        observed_seqs = Utils.get_seq_length_from_fasta(fasta)
        expected_seqs = {'chr1': 7333624, 'chr2': 6118321, 'chr3': 5939657, 'chr4': 5487742, 'chr5': 5290227, 'chr6': 4379568, 'chr7': 4147912, 'chr8': 4124671, 'chr9': 3904609, 'chr10': 2936318, 'chr11': 1275594, 'chr12': 812569, 'mito': 34391}
        self.assertDictEqual(observed_seqs,expected_seqs)

    def test_get_intergenic_coordinates(self):

        genes = Utils.extract_genes('{}/utils.genes.gff3'.format(self.data_dir))
        seqs = Utils.get_seq_length_from_fasta('{}/utils.genome.fasta'.format(self.data_dir))
        intergenic_regions = Utils.get_intergenic_coordinates(genes,seqs)
        self.assertEqual(17977,len(intergenic_regions))

    @unittest.skip("pass")
    def test_statistics(self):

        genes = Utils.extract_genes('{}/utils.genes.gff3'.format(self.data_dir))
        metrics = Utils.statistics(genes)
        self.assertEqual(17965,metrics['nb_genes'])
        self.assertEqual(17965,metrics['nb_transcripts'])

    def test_rank_transcripts(self):

        """test the ranking method

        |tr|AED tr|AED pr|rank tr|rank pr|
        |t1|  0.2 | 0.5     |       |       |
        |t2|      |      |       |       |
        |t3|
        |t4|
        |t5|
        """

        # transcripts
        t1 = Transcript("t1","chr1",10,20,1,"g1_t1")
        t2 = Transcript("t2","chr1",10,20,1,"g1_t2")
        t3 = Transcript("t3","chr1",10,20,1,"g1_t3")
        t4 = Transcript("t4","chr1",10,20,1,"g1_t4")
        t5 = Transcript("t5","chr1",10,20,1,"g1_t5")
        ltr = [t1,t2,t3,t4,t5]

        # set values
        t1.best_tr_evidence = ("unknown",0.2)
        t1.best_bx_evidence = ("unknown",0.5)
        t2.best_tr_evidence = ("unknown",0.001)
        t2.best_bx_evidence = ("unknown",0.02)
        t3.best_tr_evidence = ("unknown",0.0015)
        t3.best_bx_evidence = ("unknown",0.2)
        t4.best_tr_evidence = ("unknown",0.00001)
        t4.best_bx_evidence = ("unknown",0.1)
        t5.best_tr_evidence = ("unknown",0.1)
        t5.best_bx_evidence = ("unknown",0.0001)

        l_expected = [t4,t2,t5,t3,t1]
        l_obtained = Utils.rank_transcripts(ltr, False)
        self.assertListEqual(l_expected, l_obtained)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestUtils)
    unittest.TextTestRunner(verbosity=2).run(suite)

