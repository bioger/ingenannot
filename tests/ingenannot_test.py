#!/usr/bin/env python3
'''
common test functions
'''

import unittest
import os
from PIL import Image
import numpy as np

class IngenannotTest(unittest.TestCase):

    '''Common TestCase Class'''
        
    data_dir = '{}/../test-data'.format(os.path.abspath(os.path.dirname(__file__)))

    def setUp(self):
        pass

    @classmethod
    def are_same_files(cls, exp, obs):
        '''compare 2 files'''

        fh_exp = open(exp)
        fh_obs = open(obs)
        val = fh_exp.readlines() == fh_obs.readlines()
        fh_exp.close()
        fh_obs.close()
        return val

    @classmethod
    def are_same_image(self, exp, obs):

        img_exp = Image.open(exp)
        img_obs = Image.open(obs)
        return  np.array_equal(img_exp, img_obs)

    @classmethod
    def write_fof(self,datadir,files):

        with open("fof.cfg",'w') as f:
            for fh in files:
                if len(fh) == 2:
                    f.write("{}/{}\t{}\n".format(datadir,fh[0],fh[1]))
                if len(fh) == 3:
                    f.write("{}/{}\t{}\t{}/{}\n".format(datadir,fh[0],fh[1],
                        datadir,fh[2]))
        f.close()
        return "fof.cfg"

    @classmethod
    def write_fof_isoform(self,datadir,files):

        with open("fof.cfg",'w') as f:
            for fh in files:
                    f.write("{}/{}\t{}\t{}\n".format(datadir,fh[0],fh[1],
                        fh[2]))
        f.close()
        return "fof.cfg"


