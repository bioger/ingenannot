FROM ubuntu

# Change shell, allow source
SHELL ["/bin/bash", "-c"]
# Update apt packages
RUN apt update && \
    apt upgrade -y && \
# Install graphviz
    apt install graphviz -y && \
# Install Python3.10
    apt install python3 -y && \
# Install Pip
    apt install python3-pip -y && \
    python3 -m pip install --upgrade pip && \
# Create and Set venv
    apt install python3.10-venv -y && \
    python3 -m venv /opt/venv/cicd && \
    source /opt/venv/cicd/bin/activate && \
# Install specific modules (code, test, doc)
    pip install pylint && \
    pip install pylint-gitlab && \
    pip install anybadge && \
    pip install coverage && \
    pip install pillow && \
    pip install numpy && \
    pip install matplotlib && \
    pip install pandas && \
    pip install upsetplot && \
    pip install scikit-bio && \
    pip install pysam && \
    pip install str2bool && \
    pip install -U sphinx && \
    pip install -U sphinx-rtd-theme && \
# Clean image
    apt clean -y && \
    pip cache purge && \
    rm -rf /var/log/*
