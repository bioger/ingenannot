from ingenannot.commands.command import Command 
from ingenannot.commands.filter import Filter
from ingenannot.commands.validate import Validate
from ingenannot.commands.so_classification import SOClassification
from ingenannot.commands.compare import Compare
from ingenannot.commands.select import Select
#from ingenannot.commands.reduce import Reduce
##from ingenannot.commands.StringtieJaclip import StringtieJaclip
##from ingenannot.commands.StringtieStats import StringtieStats
##from ingenannot.commands.StringtieFilter import StringtieFilter
##from ingenannot.commands.StringtieRefine import StringtieRefine
from ingenannot.commands.utr_refine import UTRRefine
from ingenannot.commands.aed import AED
from ingenannot.commands.aed_compare import AEDCompare
from ingenannot.commands.strand_annotation_filter import StrandAnnotationFilter
#from ingenannot.commands.AddIsoforms import AddIsoforms
#from ingenannot.commands.SupportIsoformFilter import SupportIsoformFilter
from ingenannot.commands.isoform_ranking import IsoformRanking
from ingenannot.commands.rescue_effectors import RescueEffectors
from ingenannot.commands.add_sqanti3_isoforms import AddSqanti3Isoforms
from ingenannot.commands.effector_predictor_cmd import EffectorPredictorCmd
from ingenannot.commands.exonerate_to_gff import ExonerateToGff
from ingenannot.commands.rename import Rename
from ingenannot.commands.clusterize import Clusterize
from ingenannot.commands.curation import Curation
