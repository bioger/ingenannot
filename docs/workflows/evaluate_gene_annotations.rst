Evaluate gene annotations [UNDER DEVELOPMENT]
==============================================

.. _usecase_evaluate_gene_annotations:

We will propose a fully integrated Snakemake workflow to evaluate the quality of gene annotations using InGenAnnot tools. We are actively working on these workflows, and they will be released soon.


Workflows:
----------

* `ingenannot_data_workflow`: a Snakemake workflow to download proteins and RNA-Seq runs from Uniprot/SRA, respectively.
* `ingenannot_evaluation_workflow`: a Snakemake worklow to evaluate the quality of your annotation datasets compare to evidence sources.

Steps:
------

**1) Download datasets from databanks using `ingenannot_data_workflow`**


**2) Evaluate your gene annotation dataset using  `ingenannot_evaluation_workflow`**
     


Example cases:

case 1: *Saccharomyces cerevisiae*

case 2: *Botrytis cinerea*

case 3: *Arabidposis thaliana*

