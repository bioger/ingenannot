Installation
=====================

Installing with pip:
--------------------
.. code:: console

  pip install ingenannot

InGenAnnot requires several dependencies, which will be automatically installed if they are missing:

* numpy
* pysam
* matplotlib
* pandas
* upsetplot
* seaborn
* scikit-bio

It is recommended to use virtual environnements to facilitate the installation of dependencies. To do this:

.. code:: console

  # create your virtual env in a dedicated directory
  python3 -m venv $HOME/myvenvs/ingenannot

  # set up the new env
  source $HOME/myvenvs/ingenannot/bin/activate

  # install ingenannot
  pip install ingenannot 

  # enjoy
  ingenannot -h 

  # to leave the env
  deactivate

Then you will have to install all tools depending of the commands you will run (EffectorP, TargetP, SignalP, TMHMM, exonerate, samtools, miniprot, gffread, ...)

Use an Apptainer/Singularity image:
-----------------------------------

If you want to use :ref:`effector_predictor <tool_effector_predictor>` or :ref:`rescue_effector <tool_rescue_effectors>` commands, you need to install additional tools:

* EffectorP v2
* SignalP v4
* TMHMM v2
* TargetP v2

Since several of these tools are under specific licenses and with the authors' acceptance, we are unable to provide a Docker/Apptainer image with all pre-installed tools. We offer a template with predefined commands to build your own image if you have obtained the necessary licenses to use them. Below is a procedure to build this image and use it. Due to issues with pre-compiled binaries of SignalP, this image works on Linux but not on Windows with WSL2. Therefore, download the dependencies from DTU University with a license agreement (visit https://services.healthtech.dtu.dk/) and download EffectorP v2:

.. code:: console

  wget https://github.com/JanaSperschneider/EffectorP-2.0/archive/refs/tags/v2.0.1.tar.gz

Place all your archives in /tmp (you will need to bind the repository during the Apptainer build, and the path must exist in the container root filesystem to do that. Using /tmp is convenient). You should have:

.. code:: console

  ls /tmp:

* v2.0.1.tar.gz
* signalp-4.1g.Linux.tar.gz
* targetp-2.0.Linux.tar.gz
* tmhmm-2.0c.Linux.tar.gz

Then get the image definition file from InGenAnnot code and launch the build:

.. code:: console

  # get InGenAnnot code or only the image definition file
  # visit https://forgemia.inra.fr/bioger/ingenannot/-/releases and download last release
  # or download the last version of the image definition file
  wget https://forgemia.inra.fr/bioger/ingenannot/-/raw/master/ingenannot.def

  # build (singularity or apptainer)
  sudo singularity build --bind /tmp:/tmp ingenannot.sif ingenannot.def

  # test signalp
  singularity run ingenannot.sif signalp /apps/signalp-4.1/test/euk10.fsa

  # download dataset
  wget https://forgemia.inra.fr/bioger/ingenannot/-/raw/master/test-data/effpred.proteins.fasta

  # test predict effectors
  singularity run ingenannot.sif ingenannot -v 2 effector_predictor effpred.proteins.fasta

Other tools (samtools, miniprot, exonerate, StringTie, etc.) are used in the use cases proposed in this documentation. As they are not directly launched by an InGenAnnot command, we do not provide a specific container. We leave the user to handle their installation. If necessary, upon request, we will update our image to add these tools.

For any problems, bugs with InGenAnnot please open an issue on `the gitlab code repository <https://forgemia.inra.fr/bioger/ingenannot/-/issues>`_ or send me (in case of no answer on gitlab) an email to nicolas.lapalu@inrae.fr
