rename
========

.. _tool_rename:

Command :ref:`rename <tool_rename>` renames gene names.

usage
------

.. code:: console

  $ ingenannot -v 2 rename genes.gff G_{geneidx:05}0 --locus_tag --mapping


positional arguments:

=========      ================================================================================================================================
Gff_genes      Gene Annotation file in GFF/GTF file format
pattern        Pattern to use for renaming gene, {ref}=sequence, {geneidx}=gene index whole genome, {geneidxref}= gene index on the sequence
=========      ================================================================================================================================

optional arguments:

================   ====================================================================================================
-h, --help         show this help message and exit
--mapping          Export mapping file to keep track of changes
--locus_tag        Add gene Id as Locus_tag for ENA submission
--name             Replace the tag Name in Gene and mRNA feature with new ID
--tr_ranking_ID    In cases of multi-mRNA, try to keep the order based on name ex T001.1 then T001.2 when renaming
================   ====================================================================================================

inputs
------

Gff_genes in GFF/GTF format. 
Patterns and expected results for 2 genes on 2 different chromosomes (chr1, chr2):

========================  ===================================
pattern                   result 
========================  ===================================
G_{geneidx}                G_1 / G_2
G_{geneidx:05}             G_00001 / G_00002
G_{geneidx:05}0            G_000010 / G_000020
{ref}_G_{geneidx:05}0      chr1_G_000010 / chr_2G_000020
G_{ref}_{geneidxref:05}0   G_chr1_000010 / G_chr2_000010
========================  ===================================

outputs
-------

Renamed Gff file and gene / transcript mapping file if `--mapping` option.
