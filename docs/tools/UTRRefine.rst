utr_refine
==============

.. _tool_utr_refine:

Command :ref:`utr_refine <tool_utr_refine>` predicts or refine UTRs.

usage
-----

.. code:: console

  $ ingenannot -v 2 utr_refine genes.gff transcripts.gff genes.utr.gff

positional arguments:

===============       ============================================================================
Gff_genes             Gene Annotation file in GFF/GTF file format to add/correct UTRs
Gff_transcripts       Transcript Annotation file in GFF/GTF file format used to add/correct UTRs
Output                Output Annotation file in GFF file format
===============       ============================================================================

optional arguments:

======================================   ===================================================================================================================================
-h, --help                               show this help message and exit
--utr_mode {longest,shortest,rank,all}   In case of several transcripts match the ORFs to extend, select the mode to add UTR [longest, shortest, rank, all], default=longest
--erase                                  Previous annotated UTRs will be deleted, instead of to be elongated, and so possibly reduced, default=False
--onlynew                                Add only new UTR on genes without UTR, keep previous if available, default=False
======================================   ===================================================================================================================================
