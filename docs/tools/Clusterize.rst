clusterize
==========

.. _tool_clusterize:

Command :ref:`clusterize <tool_clusterize>` groups together transcripts from different transcriptome assemblies.

usage
-----

.. code:: console

  $ ingenannot clusterize Gff_transcripts Gff_out

positional arguments:

================      ======================================================
Gff_transcripts       Transcripts file in GFF/GTF file
Gff_out               Output GFF file with transcripts clusterized per gene
================      ======================================================

optional arguments:

=================================     =============================================================================================================
-h, --help                             show this help message and exit
-g,--Gff_genes GFF_GENES               Gene Annotation file in GFF/GTF file uses to remove transcripts overlapping several CDS
--keep_atts                            Keep attributes on transcript features
-r, --overlap_ratio OVERLAP_RATIO      Overlap ratio between transcripts and CDS from annotations to consider an overlap range[0.0-1.0], default=0.6
=================================     =============================================================================================================

inputs
------
Gff transcripts corresponding to a merge of several transcript assembly files with unique identifiers. Ex, transcript files obtained from StringTie with -l option.

outputs
-------

Gff file with transcripts cluterized with the same gene if overlapping.
