aed_strand_annotation_filter
==============================

.. _tool_aed_strand_annotation_filter:

Command :ref:`aed_strand_annotation_filter <tool_aed_strand_annotation_filter>` filters overlapping CDS based on their AED scores. This step is done when you perform gene selection with :ref:`select <tool_select>`. This tool is usefull to filter out results, if you use a gene predictor which predict gene on the 2 strand separetaly.

usage
------

.. code:: console

  $ ingenannot -v 2 aed_strand_annotation_filter input output


positional arguments:

=========        ===========================================
Input            GFF File with AED tags
Output           Filtered output annotation file
=========        ===========================================


optional arguments:

=================================     ======================================  
-h, --help                            show this help message and exit
=================================     ======================================


inputs
------

Gff_genes in GFF format with aed annotations.

outputs
-------

Filtered gff file, without overlapping CDS based on AED scores.

