add_sqanti3_isoforms
======================

.. _tool_add_sqanti3_isoforms:

Command :ref:`add_sqanti3_isoforms <tool_add_sqanti3_isoforms>` add isoforms to gene models from Sqanti3 annotations.

Add a defined list of isoforms annotated with Sqanti3. The isoforms must be associated with a gene contained in the gene annotation to be transfered. Warning, currently, no control will be performed on coordinates, be sure to have the good annotation. The gff file to use is the *file_corrected.gtf.cds.gff*.

usage
------

.. code:: console

  $ ingenannot -v 2 add_sqanti3_isoforms genes.gff longreads.alternatives__corrected.gtf.cds.gff lIDs --no_identicals > genes.isoforms.gff 

positional arguments:

=================     ===========================================
Gff_genes             Gene Annotation file in GFF/GTF file format
Gff_transcripts       Gff file of transcript in sqanti3 format
IDs                   File with list of transcript IDs to add
=================     ===========================================

optional arguments:
 
===================   ================================================================================================================
-h, --help            show this help message and exit
-o, --output OUTPUT   Output Annotation file in GFF file format
--no_identicals       Do not add new isoform if the same mRNA with same exons is in the annotation, even the CDS is different
===================   ================================================================================================================


inputs
------
The original annotation file with canonical gene models, the gff file with CDS annotated by Sqanti3 and the list of transcripts to add. For more info, see the usecase: :ref:`add new isoforms <usecase_add_new_isoforms>`.

outputs
-------

A gff file with isoforms.
