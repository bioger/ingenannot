validate
========

.. _tool_validate:

Command :ref:`validate <tool_validate>` checks gff consistency and performs statistics.

usage
------

.. code:: console

  $ ingenannot -v 2 validate genes.gff -s -g genome.fasta


positional arguments:

=========             ===========================================
Gff_genes             Gene Annotation file in GFF/GTF file format
=========             ===========================================

optional arguments:

==========================     =========================================================  
-h, --help                     show this help message and exit
-s, --statistics               Statistics of annotations
-g GENOME, --genome GENOME     Genome in Fasta file, more statistics metrics if provided
-a, --addseqs                  Export file in GFF3 with ##sequence-region pragma
--fixframe                     fix error in CDS frame and log warning
--gaeval                       Perform required control and transform to run with gaeval
-o OUTPUT, --output OUTPUT     Output Annotation file in GFF file format
==========================     =========================================================  

inputs
------

Gff_genes in GFF/GTF format. Common errors in annotation files are missing Parent or missing ID on some features.

outputs
-------

The first goal of :ref:`validate <tool_validate>` is to ensure the structure of your gff/gtf file with required info/tags. If you specify the :code:`-s` option you get statitistics on your annotations and more with :code:`-g`.

.. code-block:: none 

  ## Statistics ##
  nb_genes;12635
  average_gene_length;1443.0950534230312
  median_gene_length;1158
  min_gene_length;143
  max_gene_length;29360
  nb_transcripts;12635
  average_transcripts_per_gene;1.0
  average_transcript_length;1443.0950534230312
  median_transcript_length;1158
  min_transcript_length;143
  max_transcript_length;29360
  nb_exons;35603
  average_exons_per_transcript;2.8178076770874556
  average_exon_length;445.75858775945846
  median_exon_length;214
  min_exon_length;3
  max_exon_length;20704
  nb_transcript_mono_exon;3094
  nb_introns;22968
  average_introns_per_transcript;1.8178076770874554
  average_intron_length;102.88936781609195
  median_intron_length;63
  min_intron_length;40
  max_intron_length;5775
  nb_CDS;12635
  nb_complete_CDS;12635
  nb_partial_CDS;0
  average_CDS_length;1256.0417095370003
  median_CDS_length;999
  min_CDS_length;102
  max_CDS_length;29232
  nb_transcripts_with_utr;1
  average_five_prime_utr_length;90.0
  median_five_prime_utr_length;90
  min_five_prime_utr_length;90
  max_five_prime_utr_length;90
  average_three_prime_utr_length;164.0
  median_three_prime_utr_length;164
  min_three_prime_utr_length;164
  max_three_prime_utr_length;164
  ################
  
