effector_predictor
==================

.. _tool_effector_predictor:

Command :ref:`effector_predictor <tool_effector_predictor>` uses a combination of tools to predict potential fungal effector proteins.

usage
------

.. code:: console

  $ ingenannot -v 2 effector_predictor proteins.fasta


positional arguments:

========    =======================          
fasta       Fasta file of proteins
========    =======================

optional arguments:

=================================  =================================================================================
-h, --help                         show this help message and exit
--signalp SIGNALP                  Path to signalp, default=/usr/local/bin/signalp (from system lookup) 
--tmhmm TMHMM                      Path to tmhmm, default=/usr/local/bin/tmhmm-2.0c/bin/tmhmm (from system lookup)
--targetp TARGETP                  Path to targetp, default=/usr/local/bin/targetp (from system lookup)
--effectorp EFFECTORP              Path to signalp, default=None (from system lookup)
--signalp_cpos SIGNALP_CPOS        Maximal position of signal peptide cleavage site, default=25
--effectorp_score EFFECTORP_SCORE  Minimal effectorp score, default=0.7
--max_len MAX_LEN                  Maximal length of protein, default=300
--min_len MIN_LEN                  Minimal length of protein, default=30
=================================  =================================================================================

inputs
------

Fasta file of proteins

outputs
-------

`effectors.txt` (export of results from pandas dataframe 2-level index)

.. code:: console


  ,length,signalp,signalp,signalp,signalp,tmhmm,targetp,effectorp,effectorp
  ,,Cpos,Smax,SP,network,domains,Localization,prediction,probability
  Seq,,,,,,,,,
  cl_32_2,77,21,0.959,Y,SignalP-noTM,0,S,Effector,0.892
  cl_35_4,66,18,0.958,Y,SignalP-noTM,0,S,Effector,0.747
  cl_66_0,61,19,0.991,Y,SignalP-noTM,0,S,Effector,0.891

if you want to reuse this file as `pandas.DataFrame()`:

.. code-block:: python

  >> import pandas as pd
  >> df = pd.read_csv('effectors.txt', header=[0,1], index_col=0)
  >> df

                      length signalp                           tmhmm      targetp  effectorp
          Unnamed: 1_level_1    Cpos   Smax SP       network domains Localization prediction probability
  Seq
  cl_32_2                 77      21  0.959  Y  SignalP-noTM       0            S   Effector       0.892
  cl_35_4                 66      18  0.958  Y  SignalP-noTM       0            S   Effector       0.747
  cl_66_0                 61      19  0.991  Y  SignalP-noTM       0            S   Effector       0.891
