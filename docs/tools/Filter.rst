filter
========

.. _tool_filter:

Command :ref:`filter <tool_filter>` filters genes based on annotations in gff (TE, ...).

usage
------

.. code:: console

  $ ingenannot -v 2 filter genes.gff TE.gff genes.noTE.gff -f match -r 0.2


positional arguments:

=========             ===========================================
Gff_genes             Gene Annotation file in GFF/GTF file format
Gff_TEs               Annotation file in GFF/GTF file format
Output                Output Annotation file in GFF file format
=========             ===========================================


optional arguments:

=================================     =========================================================  
-h, --help                            show this help message and exit
-s SIZE, --size SIZE                  Minimum required size of feature for filtering, default=300bp
-f FEATURE, --feature FEATURE         Feature type expected for filtering, default=match_part
--bed                                 Bed file expected instead of GFF/GTF file
-r FRACTION, --fraction FRACTION      Minimum fraction of CDS overlapped for filtering, default=0.1 (10 percent)
=================================     =========================================================  


inputs
------

Gff_genes in GFF/GTF format. GFF_TEs or bed file, with features to take into account to filter the gene file

outputs
-------

Filtered gff file, with only gene structures with `gene, mRNA, exon and CDS` features. UTRs are removed.

