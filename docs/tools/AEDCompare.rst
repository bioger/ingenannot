aed_compare
============

.. _tool_aed_compare:

Command :ref:`aed_compare <tool_aed_compare>` generates graphs and computes statistics for AED comparison between different sources of annotations.

usage
------

.. code:: console

  $ ingenannot -v 2 aed_compare file.fof 


positional arguments:

=========             ===========================================
FoF                   File of files, <GFF/GTF>TAB<source>
=========             ===========================================

optional arguments:

=================     ========================================
-h, --help            show this help message and exit
-s, --statistics      Perform statistics on AED scores between sources , default=False
=================     ========================================

inputs
------
File of Files (FoF) with all files to compare. One per line such: <GFF/GTF>TAB<source>.

.. code-block:: console 

    # write compare.fof
    src1.aed.gff<TAB>run1
    src2.aed.gff<TAB>run2
    src3.aed.gff<TAB>run3

outputs
-------

The output is three graphs, showing cumulative distributions of AED scores for transcripts, proteins or best of both for all sources of dataset available in the FoF. These graphs allow comparison of AED scores between datasets. You can compute some metrics with `--statistics`

Output example of plot showing cumulative transcript AED scores:

.. image:: ../img/cumulative_tr_AED.png 
   :alt: cumulative AED plot


Output of statistics:

=====  =========  ===========  ==========  ==========  ===========  ==========  =============  =============  =============  ==================  ===============  ====================
#      mean (tr)  median (tr)  stdev (tr)  mean (pr)   median (pr)  stdev (pr)  median_geom_x  median_geom_y  mean_distance  mean_distance_to_0  median_distance  median_distance_to_0
=====  =========  ===========  ==========  ==========  ===========  ==========  =============  =============  =============  ==================  ===============  ====================
run1      0.244        0.152       0.258      0.478        0.176       0.480          0.176          0.238          0.515               0.296            0.287                 0.296
run2      0.239        0.138       0.255      0.444        0.089       0.476          0.154          0.160          0.495               0.222            0.216                 0.222
run3      0.171        0.030       0.273      0.551        1.000       0.482          0.144          0.656          0.537               0.671            0.567                 0.671
=====  =========  ===========  ==========  ==========  ===========  ==========  =============  =============  =============  ==================  ===============  ====================

Basic metrics as mean, median and stdev are provided, as well as geometric median and distance of all transcripts to this median or distance to the ideal point (0,0).
