.. InGenAnnot documentation master file, created by
   sphinx-quickstart on Wed Jul  7 17:52:13 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to InGenAnnot's documentation!
======================================

.. toctree::
   :hidden:
   :titlesonly:

   install.rst


.. toctree::
   :caption: Use cases 
   :titlesonly:
   :glob:

   usecases/*

.. toctree::
   :caption: Workflows 
   :titlesonly:
   :glob:

   workflows/*

.. toctree::
   :hidden:
   :titlesonly:
   :caption: Tools
   :glob:

   tools/*

