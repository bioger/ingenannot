Find new potential Small Secreted Proteins (SSP)
================================================

Predicting Small Secreted Proteins (SSPs) or small effectors can be challenging and requires specific tools with adapted training parameters for associated machine learning models. Few tools account for the particularities of these genes. For instance, CodingQuarry [#f1]_ may prevent the prediction of such genes despite transcriptional evidence. Here, we propose a naive tool that examines transcriptomic evidence not associated with any coding gene and attempts to predict potential SSPs based on the presence of signal peptides and other elements.


Workflow:
---------

.. graphviz::
   :name: new SSP detection workflow
   :align: center


   digraph SSP {
      "Assemble transcripts" -> "Prepare / Validate data";
      "Prepare / Validate data" -> "Rescue effector";
      "Rescue effector" -> "Select / Validate effectors";
      "Select / Validate effectors" -> "Merge with other genes";
      "Rescue effector" -> "Merge with other genes";
   }


Steps:
------

**1) Generate / Assemble new transcripts from RNA-Seq / long reads** 


Exemple: assembly of new transcripts with StringTie [#f2]_ from paired-oriented reads mapped with STAR [#f3]_:

.. code-block:: console 

    stringtie Aligned.sortedByCoord.out.bam -l 11DPI -o transcripts.gff -m 1 50 --rf -p 12 -g 0 -f 0.1 -j 4 -a 10 

**2) Prepare your data and validate them**

.. code-block:: console 

    # validate the data
    ingenannot -v 2 validate genes.gff
    ingenannot -v 2 validate transcripts.gff

    # prepare the data
    sort -k1,1 -k4g,4 transcripts.gff > transcripts.sorted.gff
    bgzip transcripts.sorted.gff
    tabix -p gff transcripts.sorted.gff.gz

**3) Run `rescue_effector` on genes with new transcripts**

.. code-block:: console 

    ingenannot -v2 rescue_effectors genes.gff transcripts.sorted.gff.gz genome.fasta

**4) Validate the new potential genes**

If no specific output file is specified, new potential effectors are saved in effectors.gff. You now have several options.

    Manual Validation: If you have a limited number of candidates, you can perform manual validation.
    Selection Step: You can perform a selection step using the same criteria as those used for gene selection with ingenannot :ref:`select <tool_select>`.If you do not have protein matches, you can create a fake one (an empty file) and follow this process:

.. code-block:: console 

    # create a fake protein evidence file
    touch proteins.empty.gff
    bgzip proteins.empty.gff
    tabix proteins.empty.gff.gz

    # select effectors based on aed score
    # write a genes.fof file such as: 
    effectors.gff3<TAB>rescue_effectors 

    # then run select, without --noaed, you perform AED annotation and selection in one step
    # You can set the thresholds for transcript AED and protein AED (see select documentation)
    ingenannot -v 2 select genes.fof effectors.select.gff3 --evtr transcripts.sorted.gff.gz --evpr proteins.empty.gff.gz --penalty_overflow 0.25 --evtrstranded --no_cds_overlap --prefix Effector

You can have a look at the distribution of aed scores in the graphics: `effectors.select.gff3.scatter_hist_aed.png`

**5) Add the new gene models**

You are now able to merge your 2 annotation files. You can simply concatenate your old annotation file with the new effectors. If you are in a full process of annotation from raw data, you can be interested to reorder/rank these new effector genes with the other genes. To do that you can you use `ingenannot selection` with no stringent selection 

.. code-block:: console 

    # write a genes.fof file such: 
    #   gene.gff<TAB>select
    #   effectors.select.gff3<TAB>rescue_effectors 
    ingenannot -v 2 select genes.fof all.genes.gff3 --noaed --no_cds_overlap --clustranded

   
References
----------

.. [#f1] Testa AC, Hane JK, Ellwood SR, Oliver RP. CodingQuarry: highly accurate hidden Markov model gene prediction in fungal genomes using RNA-seq transcripts. BMC Genomics. 2015 Mar 11;16(1):170. doi: 10.1186/s12864-015-1344-4. PMID: 25887563; PMCID: PMC4363200.
.. [#f2] Kovaka S, Zimin AV, Pertea GM, Razaghi R, Salzberg SL, Pertea M. Transcriptome assembly from long-read RNA-seq alignments with StringTie2. Genome Biol. 2019 Dec 16;20(1):278. doi: 10.1186/s13059-019-1910-1. PMID: 31842956; PMCID: PMC6912988. 
.. [#f3] Dobin A, Davis CA, Schlesinger F, Drenkow J, Zaleski C, Jha S, Batut P, Chaisson M, Gingeras TR. STAR: ultrafast universal RNA-seq aligner. Bioinformatics. 2013 Jan 1;29(1):15-21. doi: 10.1093/bioinformatics/bts635. Epub 2012 Oct 25. PMID: 23104886; PMCID: PMC3530905.

